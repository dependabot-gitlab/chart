## 3.5.0 (2025-03-08)

### 🚀 Features (1 change)

- [Add support for custom gitlab ssl certificate](dependabot-gitlab/chart@8fe0756b043531d5979b225cad6bbdd9c64b9f3f) by @andrcuns. See merge request dependabot-gitlab/chart!412

### 📦 Dependency updates (12 changes)

- [Update app version to 3.44.0-alpha.1](dependabot-gitlab/chart@babf37d4965b8a70729136bd2c7434a2209e34e9) by @dependabot-bot. See merge request dependabot-gitlab/chart!411
- [Update app version to 3.43.1-alpha.1](dependabot-gitlab/chart@4242017fe0d535fbd9f305abb7076b6b37f7a4b1) by @dependabot-bot. See merge request dependabot-gitlab/chart!410
- [Update app version to 3.43.0-alpha.1](dependabot-gitlab/chart@66e2d7e92c365f94a5b762777607c2254a8b18fd) by @dependabot-bot. See merge request dependabot-gitlab/chart!409
- [Update app version to 3.42.0-alpha.1](dependabot-gitlab/chart@b91e4297d3be2402f4438920ca0ed282ea3cb710) by @dependabot-bot. See merge request dependabot-gitlab/chart!407
- [Update Helm release redis to ~20.8.0](dependabot-gitlab/chart@61d109f422729be25223b94022528c628ce7d88c) by @dependabot-bot. See merge request dependabot-gitlab/chart!405
- [Update app version to 3.41.0-alpha.1](dependabot-gitlab/chart@dee85db1c065835cbafb04f9ee29c0d1d9f8091d) by @dependabot-bot. See merge request dependabot-gitlab/chart!404
- [Update app version to 3.40.1-alpha.1](dependabot-gitlab/chart@db90f3bd50c1f8a77a3f06ba4b9db8babbd009a9) by @dependabot-bot. See merge request dependabot-gitlab/chart!402
- [Update app version to 3.40.0-alpha.1](dependabot-gitlab/chart@c87bef2285594103b07021bb03a6323c5bd43322) by @dependabot-bot. See merge request dependabot-gitlab/chart!401
- [Update app version to 3.39.3-alpha.1](dependabot-gitlab/chart@08ce95834dcb512c2ae1651f99ceb02344fe0a6d) by @dependabot-bot. See merge request dependabot-gitlab/chart!400
- [Update app version to 3.39.2-alpha.1](dependabot-gitlab/chart@a23b8ea99d0bf48a8ee106f1f88386efa9751915) by @dependabot-bot. See merge request dependabot-gitlab/chart!399
- [Update app version to 3.39.1-alpha.1](dependabot-gitlab/chart@c66edce37166774a96e905039a57aa746a4f8a99) by @dependabot-bot. See merge request dependabot-gitlab/chart!398
- [Update Helm release redis to ~20.7.0](dependabot-gitlab/chart@e8298926c5e47d9f5dd5e5e910daf8ad282791b8) by @dependabot-bot. See merge request dependabot-gitlab/chart!397

### 🔧 CI changes (2 changes)

- [Update docker Docker tag to v28](dependabot-gitlab/chart@975d7b2334f56f5f8aea79e9d610665e177b2330) by @dependabot-bot. See merge request dependabot-gitlab/chart!406
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.22](dependabot-gitlab/chart@51f9277039ac45f73f00f8ccdb67e53d5407d272) by @dependabot-bot. See merge request dependabot-gitlab/chart!403

## 3.4.0 (2025-02-04)

### 🔬 Improvements (1 change)

- [Enabled the rendering of REDIS_URL and MONGODB_URL without trailing "." if no...](dependabot-gitlab/chart@37d4805074c69e4c8d3ab6c59aff534ca821433d) by @moebstibo. See merge request dependabot-gitlab/chart!390

### 📦 Dependency updates (5 changes)

- [Update app version to 3.39.0-alpha.1](dependabot-gitlab/chart@16d46667623a7ef41c85fe9595054e1ad0b17bfd) by @dependabot-bot. See merge request dependabot-gitlab/chart!391
- [Update Helm release redis to ~20.6.0](dependabot-gitlab/chart@c9ed4fda799e6c19804943376d5f00aa7d36808d) by @dependabot-bot. See merge request dependabot-gitlab/chart!388
- [Update app version to 3.38.0-alpha.1](dependabot-gitlab/chart@5c733e991cce2c9cf879e7173ed12a1f7d696373) by @dependabot-bot. See merge request dependabot-gitlab/chart!389
- [Update app version to 3.37.0-alpha.1](dependabot-gitlab/chart@ed0572aee5a1f479257a44495007d1357d2f6ddf) by @dependabot-bot. See merge request dependabot-gitlab/chart!387
- [Update app version to 3.35.1-alpha.1](dependabot-gitlab/chart@fe7e72387174f997cffcf683dd4a62f4b289df6c) by @dependabot-bot. See merge request dependabot-gitlab/chart!386

### 🔧 CI changes (5 changes)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.21](dependabot-gitlab/chart@6d731b97f449f639780b95ab5449d023fdd0aac4) by @dependabot-bot. See merge request dependabot-gitlab/chart!396
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm-kubeconform Docker tag to v3.17](dependabot-gitlab/chart@bd05fb2d654e9bb968be4ae094153a20adda6f90) by @dependabot-bot. See merge request dependabot-gitlab/chart!395
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm-helmdocs Docker tag to v3.17](dependabot-gitlab/chart@3e09d92717fc12031676aad96bf02b8fdda3bc4d) by @dependabot-bot. See merge request dependabot-gitlab/chart!394
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm Docker tag to v3.17](dependabot-gitlab/chart@9ccc3395d94ef5af1a15e84f0d5bb6f4d261f084) by @dependabot-bot. See merge request dependabot-gitlab/chart!393
- [Update docker Docker tag to v27.5](dependabot-gitlab/chart@0d51273c58e13376ea4b0ed6bcdeec00f66cc97c) by @dependabot-bot. See merge request dependabot-gitlab/chart!392

## 3.3.0 (2024-12-12)

### 🔬 Improvements (1 change)

- [Push charts to gitlab package registry](dependabot-gitlab/chart@5444f1dd534d47785ba63d77dab366feef6e9143) by @andrcuns. See merge request dependabot-gitlab/chart!381

### 📦 Dependency updates (9 changes)

- [Update Helm release redis to ~20.5.0](dependabot-gitlab/chart@c9d3b54fe0adfb837f960731432f4ef8d252f5dd) by @dependabot-bot. See merge request dependabot-gitlab/chart!384
- [Update Helm release mongodb to ~16.4.0](dependabot-gitlab/chart@1aeee85f219a201ac373023c7b761bf87a417207) by @dependabot-bot. See merge request dependabot-gitlab/chart!383
- [Update app version to 3.35.0-alpha.1](dependabot-gitlab/chart@600d3f398b134924392a72826366ee3188804561) by @dependabot-bot. See merge request dependabot-gitlab/chart!378
- [Update Helm release redis to ~20.4.0](dependabot-gitlab/chart@939d20eb1ea2f045137d6c6a202743c29679787a) by @dependabot-bot. See merge request dependabot-gitlab/chart!377
- [Update app version to 3.34.0-alpha.1](dependabot-gitlab/chart@1b2c48993fc95c42e22a0a89b9da2ebe78689e06) by @dependabot-bot. See merge request dependabot-gitlab/chart!376
- [Update app version to 3.33.0-alpha.1](dependabot-gitlab/chart@f840c2cfa2fc816b9bbafb7ad0cbb218ac3a6940) by @dependabot-bot. See merge request dependabot-gitlab/chart!375
- [Update Helm release mongodb to ~16.3.0](dependabot-gitlab/chart@9a9a2cac2cac3d3d6d22f94c6d1c4cd90df1d25a) by @dependabot-bot. See merge request dependabot-gitlab/chart!373
- [Update app version to 3.32.0-alpha.1](dependabot-gitlab/chart@da1c243043991b42cfaf5494c1cbc494d6c475c1) by @dependabot-bot. See merge request dependabot-gitlab/chart!374
- [Update Helm release redis to ~20.3.0](dependabot-gitlab/chart@afc6a634a35fc8b6a626d4dd10c58005f0205f23) by @dependabot-bot. See merge request dependabot-gitlab/chart!372

### 🛠️ Chore (1 change)

- [Improve chart version fetching when publishing chart](dependabot-gitlab/chart@a20ad29d01d000f56f2dd78f302e3be48e976c21) by @andrcuns.

### 🔧 CI changes (6 changes)

- [Update tool versions for install jobs](dependabot-gitlab/chart@1cf1376387174da92c21ce9aa2c5347b5ad53e63) by @andrcuns.
- [Update image for version-bump job](dependabot-gitlab/chart@5b467b3de9d5905afe4f064acb42b72990d83eeb) by @andrcuns.
- [Use custom job name when publishing index.yml](dependabot-gitlab/chart@99810690f57aabdd829c38170481e7f873ffb433) by @andrcuns. See merge request dependabot-gitlab/chart!385
- [Update docker Docker tag to v27.4](dependabot-gitlab/chart@564073feb4b6e81d0f6706581153fed5ac4edfbf) by @dependabot-bot. See merge request dependabot-gitlab/chart!382
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.20](dependabot-gitlab/chart@8e59f5e268871fcc814b6420abacb752a4a45252) by @dependabot-bot. See merge request dependabot-gitlab/chart!380
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/gsutil Docker tag to v5.32](dependabot-gitlab/chart@5638a729dc64432ed2eaad0edbb43e0e1fa67deb) by @dependabot-bot. See merge request dependabot-gitlab/chart!379

## 3.2.0 (2024-11-12)

### 🚀 Features (1 change)

- [Add option to force ssl for app access](dependabot-gitlab/chart@42d65bbc8c08ddd92af83a7a791b701b29cd8dec) by @andrcuns. See merge request dependabot-gitlab/chart!370

### 📦 Dependency updates (7 changes)

- [Update app version to 3.31.0-alpha.1](dependabot-gitlab/chart@45402d006706a60c5a23eb64a55d760b9e0314c9) by @dependabot-bot. See merge request dependabot-gitlab/chart!371
- [Update app version to 3.30.0-alpha.1](dependabot-gitlab/chart@4893c66035e55c0d20a9e249710857c3c8b51fbf) by @dependabot-bot. See merge request dependabot-gitlab/chart!369
- [Update Helm release common to ~2.27.0](dependabot-gitlab/chart@d25f1a81481c56bf7ca42a8576730d197451097e) by @dependabot-bot. See merge request dependabot-gitlab/chart!368
- [Update app version to 3.29.0-alpha.1](dependabot-gitlab/chart@44ffcd882bee16b589fa63f466f97cdd8670613d) by @dependabot-bot. See merge request dependabot-gitlab/chart!367
- [Update Helm release mongodb to ~16.2.0](dependabot-gitlab/chart@2e6c2a85e585e389a4bed0fc01531a308d563d0b) by @dependabot-bot. See merge request dependabot-gitlab/chart!366
- [Update app version to 3.28.0-alpha.1](dependabot-gitlab/chart@6d0ff890068c848d3b625f5a0c1c64e7de5fd2e7) by @dependabot-bot. See merge request dependabot-gitlab/chart!365
- [Update Helm release mongodb to ~16.1.0](dependabot-gitlab/chart@7ccc1408921ba281119697a922dfcbe33e9f4bf4) by @dependabot-bot. See merge request dependabot-gitlab/chart!364

### 🔧 CI changes (1 change)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.19](dependabot-gitlab/chart@56c8681ca99cf87457fbe95b9e63396e2215c296) by @dependabot-bot. See merge request dependabot-gitlab/chart!363

## 3.1.0 (2024-10-18)

### 📦 Dependency updates (3 changes)

- [Update app version to 3.27.0-alpha.1](dependabot-gitlab/chart@557dc27f6999d0084cdab7eb9c60f67e21089ae4) by @dependabot-bot. See merge request dependabot-gitlab/chart!362
- [Update Helm release common to ~2.26.0](dependabot-gitlab/chart@05ccc9cf180e260e30e8145bd332fe9a0d21f54f) by @dependabot-bot. See merge request dependabot-gitlab/chart!361
- [Update Helm release redis to ~20.2.0](dependabot-gitlab/chart@91153b6107674b26c8dd36e6e0a62d25295c0ce4) by @dependabot-bot. See merge request dependabot-gitlab/chart!359

### 🔧 CI changes (1 change)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/gsutil Docker tag to v5.31](dependabot-gitlab/chart@1971622b41db336a83cab8f43deb243c4cc5035c) by @dependabot-bot. See merge request dependabot-gitlab/chart!360

## 3.0.0 (2024-10-10)

### 🔬 Improvements (1 change)

- [Add option to set pathType for single host definition](dependabot-gitlab/chart@b63e556d18c09ded9b86330dddc4c34662e7b5ac) by @andrcuns. See merge request dependabot-gitlab/chart!358

### 📦 Dependency updates (2 changes)

- [Update Helm release mongodb to v16](dependabot-gitlab/chart@a369aebc624d51c09842211b637763624017d67d) by @dependabot-bot. See merge request dependabot-gitlab/chart!357
- [Update Helm release common to ~2.24.0](dependabot-gitlab/chart@a8e22a9780f072388fe60e49ee2c1b1079e3ce48) by @dependabot-bot. See merge request dependabot-gitlab/chart!356

## 2.20.0 (2024-09-26)

### 📦 Dependency updates (2 changes)

- [Update app version to 3.26.0-alpha.1](dependabot-gitlab/chart@80d6cb01fe065f7817d49c1b220745db550f0027) by @dependabot-bot. See merge request dependabot-gitlab/chart!355
- [Update app version to v3.25.0-alpha.1](dependabot-gitlab/chart@4276002aab23d5ee552435692630f058d8f7543f) by @dependabot-bot. See merge request dependabot-gitlab/chart!349

### 🔧 CI changes (2 changes)

- [Update docker Docker tag to v27.3](dependabot-gitlab/chart@ae40a394ce6cc95326601c0b8b5379d508aa163c) by @dependabot-bot. See merge request dependabot-gitlab/chart!348
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm-kubeconform Docker tag to v3.16](dependabot-gitlab/chart@00760846a66f5f20f2952af0a6f6eb81b5edaa12) by @dependabot-bot. See merge request dependabot-gitlab/chart!347

## 2.19.0 (2024-09-13)

### 📦 Dependency updates (3 changes)

- [Update app version to v3.24.0-alpha.1](dependabot-gitlab/chart@cc8677cd52f6d0e3094a424cdf3148e868aee5ae) by @dependabot-bot. See merge request dependabot-gitlab/chart!346
- [Update Helm release common to ~2.23.0](dependabot-gitlab/chart@1789c09867fdfbffb3acd473353e0a255d248291) by @dependabot-bot. See merge request dependabot-gitlab/chart!344
- [Update Helm release redis to ~20.1.0](dependabot-gitlab/chart@9f693a5fa311af6c8b92981db8af0bd92be5ccc4) by @dependabot-bot. See merge request dependabot-gitlab/chart!343

### 🔧 CI changes (2 changes)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm Docker tag to v3.16](dependabot-gitlab/chart@db55356df48a97ec7eda0e580b0ce6205434df12) by @dependabot-bot. See merge request dependabot-gitlab/chart!345
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/gsutil Docker tag to v5.30](dependabot-gitlab/chart@91ca3ee51d4a98013dfc4a868d6e4eddb02552b4) by @dependabot-bot. See merge request dependabot-gitlab/chart!342

## 2.18.0 (2024-09-05)

### 📦 Dependency updates (2 changes)

- [Update app version to v3.23.0-alpha.1](dependabot-gitlab/chart@59b916c6212a3c52fd4d46940f1c90376fa0d9d2) by @dependabot-bot. See merge request dependabot-gitlab/chart!341
- [Update app version to v3.22.0-alpha.1](dependabot-gitlab/chart@6d19bb33d3071a9a5d71ccb6e196a119831d73a8) by @dependabot-bot. See merge request dependabot-gitlab/chart!339

### 🔧 CI changes (1 change)

- [Update docker Docker tag to v27.2](dependabot-gitlab/chart@66a1df459b542eca9a2b23bfbfa439e187279eb7) by @dependabot-bot. See merge request dependabot-gitlab/chart!340

## 2.17.0 (2024-08-22)

### 📦 Dependency updates (4 changes)

- [Update app version to v3.21.0-alpha.1](dependabot-gitlab/chart@122ee36fb523b098b0a3f8cf149a7b98eab35368) by @dependabot-bot. See merge request dependabot-gitlab/chart!338
- [Update Helm release redis to v20](dependabot-gitlab/chart@68314b850e118facd4dfcba1ab76684d564750e5) by @dependabot-bot. See merge request dependabot-gitlab/chart!337
- [Update Helm release common to ~2.22.0](dependabot-gitlab/chart@df409d7631c40d38a0f2a9460a18ab3dcfe8c17a) by @dependabot-bot. See merge request dependabot-gitlab/chart!336
- [Update Helm release common to ~2.21.0](dependabot-gitlab/chart@30cc1911cdbd46dd80d065e7a4678dca77ceefd3) by @dependabot-bot. See merge request dependabot-gitlab/chart!335

## 2.16.0 (2024-08-04)

### 🔬 Improvements (1 change)

- [Add readiness probe to web pod](dependabot-gitlab/chart@c1eab58a3151b00d4c5ef20e28ae637f5d0cf913) by @andrcuns. See merge request dependabot-gitlab/chart!334

## 2.15.0 (2024-07-31)

### 📦 Dependency updates (1 change)

- [Update app version to v3.20.1-alpha.1](dependabot-gitlab/chart@b0003c28081d870a9516b4337e2d89571e7a2de0) by @dependabot-bot. See merge request dependabot-gitlab/chart!333

### 🔧 CI changes (1 change)

- [Update docker Docker tag to v27.1](dependabot-gitlab/chart@1508b111c98682feb9a0e81a2bc2418de0cf1ec5) by @dependabot-bot. See merge request dependabot-gitlab/chart!331

## 2.14.0 (2024-07-22)

### 📦 Dependency updates (2 changes)

- [Update app version to v3.19.0-alpha.1](dependabot-gitlab/chart@e69d9a2d187ddb30cbc91434b5130bf6538f270c) by @dependabot-bot. See merge request dependabot-gitlab/chart!330
- [Update app version to v3.18.1-alpha.1](dependabot-gitlab/chart@2a32d15651e6bc6ed2e3a24a5362e35e5e8e7a02) by @dependabot-bot. See merge request dependabot-gitlab/chart!329

### 🔧 CI changes (1 change)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helmdocs Docker tag to v1.14](dependabot-gitlab/chart@17302c4ac2a22c367d13928e3e92039a220a0565) by @dependabot-bot. See merge request dependabot-gitlab/chart!328

## 2.13.11 (2024-07-06)

### 🐞 Bug Fixes (1 change)

- [Fix selector label name in service monitor](dependabot-gitlab/chart@0cb19b49ec107a248648139b622242ab2d1dbcc1) by @andrcuns. See merge request dependabot-gitlab/chart!326

### 🔧 CI changes (1 change)

- [Use nightly tag in test jobs](dependabot-gitlab/chart@1aae95333ca807d6b5863b665672d7b77c64bbc2) by @andrcuns. See merge request dependabot-gitlab/chart!327

## 2.13.10 (2024-07-03)

### 📦 Dependency updates (2 changes)

- [Update app version to v3.18.0-alpha.1](dependabot-gitlab/chart@6b42673e7a125201fb39103f7a5287ab7257479b) by @dependabot-bot. See merge request dependabot-gitlab/chart!325
- [Update Helm release redis to ~19.6.0](dependabot-gitlab/chart@5be88cdaebaec62c50ba5140750ad4ccc59f6c73) by @dependabot-bot. See merge request dependabot-gitlab/chart!324

### 🔧 CI changes (1 change)

- [Update docker Docker tag to v27](dependabot-gitlab/chart@4f238bce82b39326e2f42c2fe162cb7a1a436b4d) by @dependabot-bot. See merge request dependabot-gitlab/chart!323

## 2.13.9 (2024-06-24)

### 📦 Dependency updates (1 change)

- [Update app version to v3.17.0-alpha.1](dependabot-gitlab/chart@6943e04f4f40ae845700dfd1e467ddf801ef365d) by @dependabot-bot. See merge request dependabot-gitlab/chart!322

## 2.13.8 (2024-06-20)

### 📦 Dependency updates (2 changes)

- [Update app version to v3.16.1-alpha.1](dependabot-gitlab/chart@bfeb4938d20f204f9c9231128b2edad967b1e508) by @dependabot-bot. See merge request dependabot-gitlab/chart!321
- [Update Helm release common to ~2.20.0](dependabot-gitlab/chart@9515880e001d0b996eff67c7f51102021e8bb95a) by @dependabot-bot. See merge request dependabot-gitlab/chart!320

## 2.13.7 (2024-06-03)

### 📦 Dependency updates (10 changes)

- [Update app version to v3.16.0-alpha.1](dependabot-gitlab/chart@cfa89eaef8d284726c7a5c2e3d7bf8a83f65f889) by @dependabot-bot. See merge request dependabot-gitlab/chart!319
- [Update Helm release mongodb to ~15.6.0](dependabot-gitlab/chart@d76f7e62b4663913df942f59c69382b2dc0276cb) by @dependabot-bot. See merge request dependabot-gitlab/chart!318
- [Update Helm release redis to ~19.5.0](dependabot-gitlab/chart@6ea6ab6a11e03ed674bc0e19a771103a709d61aa) by @dependabot-bot. See merge request dependabot-gitlab/chart!316
- [Update Helm release mongodb to ~15.5.0](dependabot-gitlab/chart@26ef1727f5a7017c1d07cce038cc45f661026085) by @dependabot-bot. See merge request dependabot-gitlab/chart!312
- [Update Helm release redis to ~19.4.0](dependabot-gitlab/chart@7ef3cad0652a5291a644ac23eb98969f0a556f27) by @dependabot-bot. See merge request dependabot-gitlab/chart!313
- [Update Helm release mongodb to ~15.4.0](dependabot-gitlab/chart@d33546532f7c7dfc5ca6a0ae718633617dd896b1) by @dependabot-bot. See merge request dependabot-gitlab/chart!311
- [Update Helm release mongodb to ~15.3.0](dependabot-gitlab/chart@b0c17ff0ad0534ee55ce864bb9446724d97ba340) by @dependabot-bot. See merge request dependabot-gitlab/chart!309
- [Update Helm release mongodb to ~15.2.0](dependabot-gitlab/chart@db55cf55f087dd2dec15fa841861e91737c6f31d) by @dependabot-bot. See merge request dependabot-gitlab/chart!308
- [Update Helm release redis to ~19.3.0](dependabot-gitlab/chart@b44ce12863c1f417c89402f5ca5422de4e958d7e) by @dependabot-bot. See merge request dependabot-gitlab/chart!307
- [Update Helm release redis to ~19.2.0](dependabot-gitlab/chart@e21ea675e6d9f8217135b9d0a144a4677dc3fd91) by @dependabot-bot. See merge request dependabot-gitlab/chart!305

### 🔧 CI changes (5 changes)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm-kubeconform Docker tag to v3.15](dependabot-gitlab/chart@642450e153164199eb1ba65fe9f2db407b96fc7a) by @dependabot-bot. See merge request dependabot-gitlab/chart!317
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm Docker tag to v3.15](dependabot-gitlab/chart@53463bc374a0f80e5e415cd4ed3d05ad94a9f5fa) by @dependabot-bot. See merge request dependabot-gitlab/chart!315
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/gsutil Docker tag to v5.29](dependabot-gitlab/chart@b7ef32d1e86cbb805b078036728ade5d0e36875d) by @dependabot-bot. See merge request dependabot-gitlab/chart!314
- [Fix app image tag used in installation tests](dependabot-gitlab/chart@fbcdee978684fec836184e21010868787c974a3b) by @andrcuns. See merge request dependabot-gitlab/chart!310
- [Remove docker runner tag](dependabot-gitlab/chart@463b7b4df7bbe86b627427139a550adde10b82f6) by @andrcuns. See merge request dependabot-gitlab/chart!306

## 2.13.6 (2024-05-06)

### 📦 Dependency updates (2 changes)

- [Update app version to v3.15.0-alpha.1](dependabot-gitlab/chart@1b7b26fc7d2649d55fd5cb98627298d7f133e6fc) by @dependabot-bot. See merge request dependabot-gitlab/chart!304
- [Update app version to v3.14.8-alpha.1](dependabot-gitlab/chart@3e8ec1caf7e91a12afc83f5591c185c5777cb269) by @dependabot-bot. See merge request dependabot-gitlab/chart!301

### 🔧 CI changes (2 changes)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/gsutil Docker tag to v5.28](dependabot-gitlab/chart@b3341ebe055fb57302713dc68b8250bfad5f9ec4) by @dependabot-bot. See merge request dependabot-gitlab/chart!303
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.18](dependabot-gitlab/chart@fd00bce281db84be7b927d2c94aad7ff78f7417a) by @dependabot-bot. See merge request dependabot-gitlab/chart!302

## 2.13.5 (2024-04-24)

### 📦 Dependency updates (3 changes)

- [Update app version to v3.14.7-alpha.1](dependabot-gitlab/chart@8862054d9cd6c8366078a2472230e8c1dffcd4e9) by @dependabot-bot. See merge request dependabot-gitlab/chart!299
- [Update app version to v3.14.6-alpha.1](dependabot-gitlab/chart@33d34879f609f7f02fad3c73ef32f2bc8ee3c293) by @dependabot-bot. See merge request dependabot-gitlab/chart!298
- [Update Helm release redis to ~19.1.0](dependabot-gitlab/chart@10c5f7cf5053df08996063d50c530b5b77146c63) by @dependabot-bot. See merge request dependabot-gitlab/chart!296

### 🔧 CI changes (2 changes)

- [Update docker Docker tag to v26.1](dependabot-gitlab/chart@e76b12aa851eacc2e89c474de95f6c9ee57e2d66) by @dependabot-bot. See merge request dependabot-gitlab/chart!300
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.17](dependabot-gitlab/chart@653f6783e8d05f2d726463e5ef66c3f5cf940714) by @dependabot-bot. See merge request dependabot-gitlab/chart!297

## 2.13.4 (2024-04-07)

### 📦 Dependency updates (1 change)

- [Update app version to v3.14.5-alpha.1](dependabot-gitlab/chart@0d724679522f469cbec86cc30003310a414ec68c) by @dependabot-bot. See merge request dependabot-gitlab/chart!295

## 2.13.3 (2024-03-30)

### 📦 Dependency updates (8 changes)

- [Update app version to v3.14.4-alpha.1](dependabot-gitlab/chart@9b2c9ecdbf2283e40c32873f009b3ccf5518ada8) by @dependabot-bot. See merge request dependabot-gitlab/chart!294
- [Update Helm release mongodb to ~15.1.0](dependabot-gitlab/chart@95e01bab89824179f3d160d6ad0b8ba88aafe67f) by @dependabot-bot. See merge request dependabot-gitlab/chart!293
- [Update app version to v3.14.3-alpha.1](dependabot-gitlab/chart@de8214acb144c0dc6b500133ed851e8fcda7054f) by @dependabot-bot. See merge request dependabot-gitlab/chart!290
- [Update Helm release redis to v19](dependabot-gitlab/chart@66de2b146f59584a54cec9349e575569672528f5) by @dependabot-bot. See merge request dependabot-gitlab/chart!291
- [Update Helm release mongodb to v15](dependabot-gitlab/chart@0858df0a690ac8aaf55b755bf1b6d20c37adf234) by @dependabot-bot. See merge request dependabot-gitlab/chart!289
- [Update Helm release redis to ~18.19.0](dependabot-gitlab/chart@12a17ca0f190197f47f8509bc7690b15ffc487da) by @dependabot-bot. See merge request dependabot-gitlab/chart!288
- [Update Helm release common to ~2.19.0](dependabot-gitlab/chart@d8723d5d46fabfc7f2b971f97b308d3a29a42ded) by @dependabot-bot. See merge request dependabot-gitlab/chart!287
- [Update Helm release mongodb to ~14.13.0](dependabot-gitlab/chart@c5c3b02299d326a93ab156ab145aaa38222f138d) by @dependabot-bot. See merge request dependabot-gitlab/chart!286

### 🔧 CI changes (1 change)

- [Update docker Docker tag to v26](dependabot-gitlab/chart@211966d3ca3a824b7a2a4e2f83914a9746795bd0) by @dependabot-bot. See merge request dependabot-gitlab/chart!292

## 2.13.2 (2024-03-07)

### 📦 Dependency updates (3 changes)

- [Update app version to v3.14.2-alpha.1](dependabot-gitlab/chart@e207e25d380e265a102090f1da0960342bf3d455) by @dependabot-bot. See merge request dependabot-gitlab/chart!285
- [Update Helm release redis to ~18.18.0](dependabot-gitlab/chart@36d0d7372c3768c4279b488cf8dfc0c6e796b3d0) by @dependabot-bot. See merge request dependabot-gitlab/chart!284
- [Update Helm release common to ~2.18.0](dependabot-gitlab/chart@59a3477f037f2ac791c530c7276f9ae8d2cc2bd0) by @dependabot-bot. See merge request dependabot-gitlab/chart!283

## 2.13.1 (2024-02-27)

### 📦 Dependency updates (4 changes)

- [Update Helm release redis to ~18.17.0](dependabot-gitlab/chart@78ddb6ee258be5b020d9dbd34ea96da6667532c4) by @dependabot-bot. See merge request dependabot-gitlab/chart!282
- [Update app version to v3.14.1-alpha.1](dependabot-gitlab/chart@0f255aad45107ceefa5b43da9b1b72156b4a12b3) by @dependabot-bot. See merge request dependabot-gitlab/chart!281
- [Update Helm release mongodb to ~14.12.0](dependabot-gitlab/chart@563c6b183d11c868835608c1868ed2416f12507c) by @dependabot-bot. See merge request dependabot-gitlab/chart!278
- [Update Helm release redis to ~18.16.0](dependabot-gitlab/chart@fdeb4213443b5aeda02bbd61a378b458fa4af4ab) by @dependabot-bot. See merge request dependabot-gitlab/chart!279

### 🔧 CI changes (1 change)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helmdocs Docker tag to v1.13](dependabot-gitlab/chart@25bac28da92d26f1419d47e2dd021d81433ea492) by @dependabot-bot. See merge request dependabot-gitlab/chart!280

## 2.13.0 (2024-02-17)

### 🔬 Improvements (1 change)

- [Changed nindent values from 12 to 4 to make indentation work when templating](dependabot-gitlab/chart@bd317f795741cbaa269a3772ae2bfb3d60561397) by @mortenbruhn1. See merge request dependabot-gitlab/chart!270

### 📦 Dependency updates (13 changes)

- [Update app version to v3.14.0-alpha.1](dependabot-gitlab/chart@493e6484223d7ea5474e186d6553c7dea2a9c7cf) by @dependabot-bot. See merge request dependabot-gitlab/chart!277
- [Update Helm release redis to ~18.14.0](dependabot-gitlab/chart@b94d43a541037fa58a12d14a6d94dc9866c4b741) by @dependabot-bot. See merge request dependabot-gitlab/chart!276
- [Update Helm release mongodb to ~14.10.0](dependabot-gitlab/chart@5b05a1284fe96f2be008e9cce0ea6843940890d6) by @dependabot-bot. See merge request dependabot-gitlab/chart!275
- [Update Helm release mongodb to ~14.9.0](dependabot-gitlab/chart@d606947841708c15ce8a773a77139f3db641adde) by @dependabot-bot. See merge request dependabot-gitlab/chart!274
- [Update Helm release common to ~2.16.0](dependabot-gitlab/chart@58080a7b16918fb1c5ee84c055d52d28ee50ff8d) by @dependabot-bot. See merge request dependabot-gitlab/chart!273
- [Update Helm release redis to ~18.13.0](dependabot-gitlab/chart@b7e81768f6bda394eb8359a3affb1b819a3f8b6f) by @dependabot-bot. See merge request dependabot-gitlab/chart!272
- [Update Helm release common to ~2.15.0](dependabot-gitlab/chart@bf94bca5437ab6fd26ecd055f9ad6d4e85057d28) by @dependabot-bot. See merge request dependabot-gitlab/chart!271
- [Update Helm release mongodb to ~14.8.0](dependabot-gitlab/chart@45a9ede7a5c809023149117d499c96a2d454bba4) by @dependabot-bot. See merge request dependabot-gitlab/chart!269
- [Update Helm release redis to ~18.12.0](dependabot-gitlab/chart@4766fe891a99667f05b32453baec8e0477c2b8f2) by @dependabot-bot. See merge request dependabot-gitlab/chart!268
- [Update Helm release mongodb to ~14.7.0](dependabot-gitlab/chart@0a39b8b9cadafe1b3f4a33d08e1a69b53bb2c0fd) by @dependabot-bot. See merge request dependabot-gitlab/chart!266
- [Update Helm release redis to ~18.8.0](dependabot-gitlab/chart@f001c95bd934d4d3c93d6f0a72ba53dae514cb8f) by @dependabot-bot. See merge request dependabot-gitlab/chart!267
- [Update Helm release mongodb to ~14.6.0](dependabot-gitlab/chart@dfb74d91a0cd977155e7dc59ed0759f9eed174c8) by @dependabot-bot. See merge request dependabot-gitlab/chart!262
- [Update Helm release redis to ~18.7.0](dependabot-gitlab/chart@14fc07b7f3cc91de4a378cfe0c1cc3d5c482060e) by @dependabot-bot. See merge request dependabot-gitlab/chart!261

### 🔧 CI changes (3 changes)

- [Update docker Docker tag to v25](dependabot-gitlab/chart@4c10a2e633391c3056ad9a971b15f3782e2bfbe3) by @dependabot-bot. See merge request dependabot-gitlab/chart!265
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm-kubeconform Docker tag to v3.14](dependabot-gitlab/chart@0092303e2bf1483d4df503c9d2fdac164c5506b9) by @dependabot-bot. See merge request dependabot-gitlab/chart!264
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm Docker tag to v3.14](dependabot-gitlab/chart@2107e61ae44690e99c5c3d88bea35b42d30643a2) by @dependabot-bot. See merge request dependabot-gitlab/chart!263

## 2.12.2 (2024-01-14)

### 📦 Dependency updates (1 change)

- [Update app version to v3.13.3-alpha.1](dependabot-gitlab/chart@0581951e6c691ce76ffdef49b28785fa0a15fa32) by @dependabot-bot. See merge request dependabot-gitlab/chart!260

## 2.12.1 (2024-01-08)

### 📦 Dependency updates (2 changes)

- [Update app version to v3.13.2-alpha.1](dependabot-gitlab/chart@3485d6e9dd4da776a1070c5d6494b8e30a0b96e2) by @dependabot-bot. See merge request dependabot-gitlab/chart!259
- [Update Helm release mongodb to ~14.5.0](dependabot-gitlab/chart@0a5b1591660879e3baf193935d8f38a23b2fe83f) by @dependabot-bot. See merge request dependabot-gitlab/chart!258

## 2.12.0 (2023-12-29)

### 📦 Dependency updates (2 changes)

- [Update app version to v3.13.1-alpha.1](dependabot-gitlab/chart@6ae5ac7cc3630d9c16b498ffef31e8e662941b82) by @dependabot-bot. See merge request dependabot-gitlab/chart!257
- [Update app version to v3.13.0-alpha.1](dependabot-gitlab/chart@2fa553aa2e88d977f5ef3e94e72c17492f8ded9c) by @dependabot-bot. See merge request dependabot-gitlab/chart!256

### 🔧 CI changes (1 change)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helmdocs Docker tag to v1.12](dependabot-gitlab/chart@06733401ff11e34528a48cbb7574f7b1aaac5455) by @dependabot-bot. See merge request dependabot-gitlab/chart!255

## 2.11.0 (2023-12-22)

### 📦 Dependency updates (6 changes)

- [Update app version to v3.12.0-alpha.1](dependabot-gitlab/chart@cc7be1683b98e5c39908292fdbfae0438356e103) by @dependabot-bot. See merge request dependabot-gitlab/chart!254
- [Update Helm release redis to ~18.6.0](dependabot-gitlab/chart@040b275c67ff0672cba490fc548725aa1f27026b) by @dependabot-bot. See merge request dependabot-gitlab/chart!253
- [Update Helm release common to ~2.14.0](dependabot-gitlab/chart@4e0335bb2871dca44ce3115a898369dd4b58cd0e) by @dependabot-bot. See merge request dependabot-gitlab/chart!252
- [Update Helm release redis to ~18.5.0](dependabot-gitlab/chart@1a41b5da50b39f7c144142fa890d6e67a9f01dcc) by @dependabot-bot. See merge request dependabot-gitlab/chart!251
- [Update app version to v3.11.0-alpha.1](dependabot-gitlab/chart@2001888f3a8ff0990ef17f3413f8969fd2bcf28e) by @dependabot-bot. See merge request dependabot-gitlab/chart!250
- [Update Helm release mongodb to ~14.4.0](dependabot-gitlab/chart@7da3f4f1c04ceb59d2e19eb23f1efed4c4919151) by @dependabot-bot. See merge request dependabot-gitlab/chart!249

## 2.10.0 (2023-12-03)

### 🔬 Improvements (1 change)

- [Allow to add labels on ingress](dependabot-gitlab/chart@71b124077f5b33ce29b95067a5f44b3429d60a0f) by @xmichel2. See merge request dependabot-gitlab/chart!247

### 📦 Dependency updates (3 changes)

- [Update app version to v3.10.0-alpha.1](dependabot-gitlab/chart@460c4e567099f42373efab112577530438403c9f) by @dependabot-bot. See merge request dependabot-gitlab/chart!246
- [Update app version to v3.9.2-alpha.1](dependabot-gitlab/chart@c93940b9ff1fb894b6cd30fe209d01e87a135ca4) by @dependabot-bot. See merge request dependabot-gitlab/chart!245
- [Update app version to v3.9.1-alpha.1](dependabot-gitlab/chart@3dbc16076c1131cf58efa383eb6b9944d61788ef) by @dependabot-bot. See merge request dependabot-gitlab/chart!243

### 🔧 CI changes (5 changes)

- [Add labels to ingress test values](dependabot-gitlab/chart@a3a113a47550a8cdc2560a8a9a5edc77d606e97e) by @andrcuns. See merge request dependabot-gitlab/chart!248
- [Make version bump job only manual](dependabot-gitlab/chart@a4b825b920cb5b33660adb91e602e730d1a9a6a9) by @andrcuns.
- [Add options to semver component input](dependabot-gitlab/chart@ad61ebefb88363d7c80e2e9d7f5f48b6d93e4f60) by @andrcuns.
- [Fix regex for app update commit message](dependabot-gitlab/chart@54c22d488f39d23770bab4426f163035cfa711ec) by @andrcuns. See merge request dependabot-gitlab/chart!244
- [Don't interrupt release jobs](dependabot-gitlab/chart@f2547e1f95bdc2c5db14b776774360be91ee1031) by @andrcuns.

## 2.9.0 (2023-11-26)

### 📦 Dependency updates (2 changes)

- [Update app version to v3.9.0-alpha.1](dependabot-gitlab/chart@bdd0729631dd2f0cb11a3a20b499a242424beddf) by @dependabot-bot. See merge request dependabot-gitlab/chart!241
- [Update Helm release mongodb to ~14.3.0](dependabot-gitlab/chart@019e450ac2044b5cfa6900eebab1f55de6faaa24) by @dependabot-bot. See merge request dependabot-gitlab/chart!240

### 🔧 CI changes (1 change)

- [Add version bump job](dependabot-gitlab/chart@25ed4a8d544abf44599a6af27553041b34cff340) by @andrcuns. See merge request dependabot-gitlab/chart!242

## 2.8.0 (2023-11-19)

### 📦 Dependency updates (5 changes)

- [Update app version to v3.8.0-alpha.1](dependabot-gitlab/chart@fd25a253df5df695ad13294910be498116a35698) by @dependabot-bot. See merge request dependabot-gitlab/chart!238
- [Update Helm release redis to ~18.4.0](dependabot-gitlab/chart@89f64f4f464a0f13d525c7da2064976e132ef61b) by @dependabot-bot. See merge request dependabot-gitlab/chart!237
- [Update Helm release mongodb to ~14.2.0](dependabot-gitlab/chart@8850ee8f8fbab2aa78d543e8df950d719a5a179b) by @dependabot-bot. See merge request dependabot-gitlab/chart!235
- [Update Helm release redis to ~18.3.0](dependabot-gitlab/chart@7fd48d12956e9eb8351c6a01dc254c08cb890b61) by @dependabot-bot. See merge request dependabot-gitlab/chart!236
- [Update Helm release mongodb to ~14.1.0](dependabot-gitlab/chart@8956c0ad28e891092b0f66fd88438452c110b51a) by @dependabot-bot. See merge request dependabot-gitlab/chart!234

### 🔧 CI changes (1 change)

- [Update chart testing ](dependabot-gitlab/chart@546f7be9cc8a456dda5ed0589e8759b8a0f76be7) by @andrcuns. See merge request dependabot-gitlab/chart!239

## 2.7.0 (2023-10-29)

### 📦 Dependency updates (2 changes)

- [Update app version to v3.7.0-alpha.1](dependabot-gitlab/chart@056b5c7f2f5397212cf153f3fab28c47c05ceb71) by @dependabot-bot. See merge request dependabot-gitlab/chart!233
- [Update Helm release redis to ~18.2.0](dependabot-gitlab/chart@0cf70ca26b24c4dd0fbd8a05c864a5b630746b76) by @dependabot-bot. See merge request dependabot-gitlab/chart!231

### 🔧 CI changes (1 change)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/gsutil Docker tag to v5.27](dependabot-gitlab/chart@268642d77d3a561427dae416ede5c0e9aca248fa) by @dependabot-bot. See merge request dependabot-gitlab/chart!232

## 2.6.0 (2023-10-16)

### 📦 Dependency updates (1 change)

- [Update app version to v3.6.0-alpha.1](dependabot-gitlab/chart@4cadfd9d7276ebe9cc249a148b02af9c49a24db2) by @dependabot-bot. See merge request dependabot-gitlab/chart!230

## 2.5.0 (2023-10-03)

### 📦 Dependency updates (3 changes)

- [Update app version to v3.5.0-alpha.1](dependabot-gitlab/chart@70384487f37af1199a63d0e42f284f2ec313369f) by @dependabot-bot. See merge request dependabot-gitlab/chart!229
- [Update Helm release common to ~2.13.0](dependabot-gitlab/chart@0beda32d3d75a56d65639d98ed5a11a8527e8e7f) by @dependabot-bot. See merge request dependabot-gitlab/chart!228
- [Update Helm release mongodb to v14](dependabot-gitlab/chart@c37c57f8324ac6d73b4ada9b4432954a13078c56) by @dependabot-bot. See merge request dependabot-gitlab/chart!225

### 🔧 CI changes (2 changes)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm-kubeconform Docker tag to v3.13](dependabot-gitlab/chart@2991ebaec07c439e0cd731623c761301243f6554) by @dependabot-bot. See merge request dependabot-gitlab/chart!227
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm Docker tag to v3.13](dependabot-gitlab/chart@6118de386fbe48b2bc7b528c732c28f9aa21a5e0) by @dependabot-bot. See merge request dependabot-gitlab/chart!226

## 2.4.0 (2023-09-28)

### 📦 Dependency updates (4 changes)

- [Update Helm release redis to ~18.1.0](dependabot-gitlab/chart@4d7ca52f001a80bde6f5a40b18d4ee4dbee96013) by @dependabot-bot. See merge request dependabot-gitlab/chart!222
- [Update app version to v3.4.0-alpha.1](dependabot-gitlab/chart@e65d588b24435d35c99353aeeab0e2204a905ea3) by @dependabot-bot. See merge request dependabot-gitlab/chart!223
- [Update Helm release common to ~2.12.0](dependabot-gitlab/chart@0f23d45aa705e7b929d0551c70af758eab845764) by @dependabot-bot. See merge request dependabot-gitlab/chart!221
- [Update Helm release common to ~2.11.0](dependabot-gitlab/chart@4b43333f99be749d89a4ccf754e872e68601d70e) by @dependabot-bot. See merge request dependabot-gitlab/chart!220

### 🔧 CI changes (1 change)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.16](dependabot-gitlab/chart@81e0fb8997f1db91e2afe6bd45237c67202a03e6) by @dependabot-bot. See merge request dependabot-gitlab/chart!224

## 2.3.0 (2023-09-14)

### 📦 Dependency updates (2 changes)

- [Update app version to v3.3.0-alpha.1](dependabot-gitlab/chart@86ac0392e117ffa167cfdc50fbfd83f9bafb4ab3) by @dependabot-bot. See merge request dependabot-gitlab/chart!219
- [Update Helm release mongodb to ~13.18.0](dependabot-gitlab/chart@fc20695329104d7779c8e59a383d1caab8a8146b) by @dependabot-bot. See merge request dependabot-gitlab/chart!218

## 2.2.0 (2023-09-10)

### 📦 Dependency updates (1 change)

- [Update app version to v3.2.0-alpha.1](dependabot-gitlab/chart@5f711c3050c382ba272ea071f255cb46c70b18d8) by @dependabot-bot. See merge request dependabot-gitlab/chart!217

## 2.1.0 (2023-09-07)

### 📦 Dependency updates (1 change)

- [Update app version to v3.1.0-alpha.1](dependabot-gitlab/chart@cb7238c48dd4aa22f6ba494f8e9a67b6f23ade76) by @dependabot-bot. See merge request dependabot-gitlab/chart!216

## 2.0.0 (2023-09-06)

### 💥 Breaking changes (1 change)

- [Update metrics setup](dependabot-gitlab/chart@955ca153a8d31a33d40e82ab25151a900fda215a) by @andrcuns. See merge request dependabot-gitlab/chart!212

### 🐞 Bug Fixes (1 change)

- [Fix updater pod template indentation](dependabot-gitlab/chart@332d140f2923eb18af9b709b64800032ea7f6fee) by @andrcuns. See merge request dependabot-gitlab/chart!213

### 📦 Dependency updates (2 changes)

- [Update app version to v3.0.0-alpha.1](dependabot-gitlab/chart@8bea3960bacc66c41d07158ecba83d25a99eccaf) by @dependabot-bot. See merge request dependabot-gitlab/chart!215
- [Update Helm release redis to v18](dependabot-gitlab/chart@b45c14d730db7b981d9eefd12ca29a20899eaa5d) by @dependabot-bot. See merge request dependabot-gitlab/chart!210

### 🛠️ Chore (1 change)

- [Remove pvc for metrics direct store](dependabot-gitlab/chart@fac8281ee2f2357b4b7f4f8930a3c193ed3b61c1) by @andrcuns. See merge request dependabot-gitlab/chart!214

## 1.11.1 (2023-08-30)

### 📦 Dependency updates (1 change)

- [Update app version to v2.0.0-alpha.5](dependabot-gitlab/chart@ce1ae3fc87b41ef6c683f3fe6695fe43aa6b4d3a) by @dependabot-bot. See merge request dependabot-gitlab/chart!211

## 1.11.0 (2023-08-27)

### 🔬 Improvements (1 change)

- [Add job for handling background post deployment tasks](dependabot-gitlab/chart@47bcba6dddbcf0b7170ccbfd4dd7f5fbbe98008e) by @andrcuns. See merge request dependabot-gitlab/chart!207

### 📦 Dependency updates (1 change)

- [Update app version to v2.0.0-alpha.4](dependabot-gitlab/chart@73193b9dad70fc0f7797b4d0b3629b31df3a290d) by @dependabot-bot. See merge request dependabot-gitlab/chart!208

### 🛠️ Chore (1 change)

- [Update rake task names](dependabot-gitlab/chart@3e4501ee6a99630ca494fd32cd3a735a1013d94d) by @andrcuns. See merge request dependabot-gitlab/chart!206

### 🔧 CI changes (1 change)

- [Introduce breaking changelog-category](dependabot-gitlab/chart@c3a8287bd3e019adf68e9b7e7235b24db2394bb4) by @andrcuns. See merge request dependabot-gitlab/chart!205

## 1.10.0 (2023-08-21)

### 🔬 Improvements (2 changes)

- [Add support for explicit container security configuration](dependabot-gitlab/chart@3a0bda04c37b2d9aa46f3290e8ed7adc22cb8cb6) by @andrcuns. See merge request dependabot-gitlab/chart!204
- [Add sentry profile sample rate parameter](dependabot-gitlab/chart@f8106baa5abf4b9f2290e8c76d0acdf753466b89) by @andrcuns. See merge request dependabot-gitlab/chart!201

### 🐞 Bug Fixes (1 change)

- [Quote traces and profiles sample rate config values](dependabot-gitlab/chart@daa816ca223f1f6bed6e61331464666267f1d6ab) by @andrcuns. See merge request dependabot-gitlab/chart!202

### 📦 Dependency updates (1 change)

- [chore(deps): update helm-chart-dependencies](dependabot-gitlab/chart@4ac8f89e56300eef7e96b1dc19c6d4c2a4c2e8d1) by @dependabot-bot. See merge request dependabot-gitlab/chart!203

### 🔧 CI changes (1 change)

- [Introduce breaking changelog-category](dependabot-gitlab/chart@c3a8287bd3e019adf68e9b7e7235b24db2394bb4) by @andrcuns. See merge request dependabot-gitlab/chart!205

## 1.9.0 (2023-08-05)

### 📦 Dependency updates (5 changes)

- [Update app version to v2.0.0-alpha.3](dependabot-gitlab/chart@a5f27b1dbe4b2fe82be79fa0fdbdf2ebf64c34b9) by @dependabot-bot. See merge request dependabot-gitlab/chart!200
- [chore(deps): update helm release redis to ~17.14.0](dependabot-gitlab/chart@a4664b80c9b4b388a0e6e3afa98af6f312611714) by @dependabot-bot. See merge request dependabot-gitlab/chart!199
- [Update app version to v2.0.0-alpha.2](dependabot-gitlab/chart@cc9191127094d11b87562b9e8cda82a77d815ad2) by @dependabot-bot. See merge request dependabot-gitlab/chart!198
- [chore(deps): update helm release mongodb to ~13.16.0](dependabot-gitlab/chart@4c8822d142cd3ea59e168bc4eeb8d9ebcb915299) by @dependabot-bot. See merge request dependabot-gitlab/chart!197
- [chore(deps): update helm release redis to ~17.13.0](dependabot-gitlab/chart@179a33f675a7c9a0f805539d5d8d5dc39be00ba0) by @dependabot-bot. See merge request dependabot-gitlab/chart!196

## 1.8.0 (2023-07-17)

### 📦 Dependency updates (4 changes)

- [Update app version to v2.0.0-alpha.1](dependabot-gitlab/chart@4d3bb1535a81d332d7a431fd1b2b344aa0a8a15c) by @dependabot-bot. See merge request dependabot-gitlab/chart!195
- [Update app version to v1.0.0-alpha.10](dependabot-gitlab/chart@ab35486942ef123c7816fcbb383db7b6646679f0) by @dependabot-bot. See merge request dependabot-gitlab/chart!194
- [chore(deps): update helm release common to ~2.6.0](dependabot-gitlab/chart@76a627988eae1ddaf01dd396cadaa600d3069015) by @dependabot-bot. See merge request dependabot-gitlab/chart!192
- [chore(deps): update helm release common to ~2.5.0](dependabot-gitlab/chart@b771ee4f6ea4ea3cd03ff42157f75b0ddbf814d2) by @dependabot-bot. See merge request dependabot-gitlab/chart!191

## 1.7.0 (2023-06-06)

### 🚀 Features (2 changes)

- [feat: make it possible to specify ingress path type](dependabot-gitlab/chart@3f94cfdf77f5de5aa5a188f016ef2996089ac6e2) by @argoyle. See merge request dependabot-gitlab/chart!190
- [feat: add lifecycle to updater-pod](dependabot-gitlab/chart@18ebabe3a337a8331af2632431db2a7dbef06fa7) by @argoyle. See merge request dependabot-gitlab/chart!189

## 1.6.0 (2023-05-29)

### 📦 Dependency updates (3 changes)

- [Update Helm release mongodb to ~13.15.0](dependabot-gitlab/chart@1cf2dbb1ede49922fbf33812b01cdfff67d19f1f) by @dependabot-bot. See merge request dependabot-gitlab/chart!186
- [Update app version to v1.0.0-alpha.9](dependabot-gitlab/chart@ec231494e7a19992e14c35c6669595bccf5fbf51) by @dependabot-bot. See merge request dependabot-gitlab/chart!188
- [Update Helm release common to v2](dependabot-gitlab/chart@0fcfa6b0930f5c9a76d6b5f926c21f978a9fc1e3) by @dependabot-bot. See merge request dependabot-gitlab/chart!187

## 1.5.0 (2023-05-23)

### 📦 Dependency updates (2 changes)

- [Update app version to v1.0.0-alpha.8](dependabot-gitlab/chart@7574c120d5d9e51951e361e52c097eabdd0af249) by @dependabot-bot. See merge request dependabot-gitlab/chart!185
- [Update app version to v1.0.0-alpha.7](dependabot-gitlab/chart@c4afaa919f39d63d1a8323b5bdd4fd1a402b65bf) by @dependabot-bot. See merge request dependabot-gitlab/chart!184

### 🔧 CI changes (1 change)

- [Update ci images](dependabot-gitlab/chart@9767eb8b5dc41de93ddf81bb9d228927b9135ebb) by @andrcuns. See merge request dependabot-gitlab/chart!182

## 1.4.0 (2023-05-19)

### 📦 Dependency updates (4 changes)

- [Update Helm release mongodb to ~13.13.0](dependabot-gitlab/chart@d75285b5356485ebd08f5a266ccdb45d0dc06eac) by @dependabot-bot. See merge request dependabot-gitlab/chart!180
- [Update Helm release redis to ~17.11.0](dependabot-gitlab/chart@03a3f3929aab3557b502d230cd5d1f1ed376eb1d) by @dependabot-bot. See merge request dependabot-gitlab/chart!181
- [Update app version to v1.0.0-alpha.6](dependabot-gitlab/chart@84681284193eb10d9c058cb28b9edc1440030c5b) by @dependabot-bot. See merge request dependabot-gitlab/chart!179
- [Update app version to v1.0.0-alpha.5](dependabot-gitlab/chart@92c1bef0a54a339c8435ee18a3b24a82da446c7b) by @dependabot-bot. See merge request dependabot-gitlab/chart!178

## 1.3.0 (2023-05-14)

### 🔬 Improvements (1 change)

- [Adds pod annotations to migration job](dependabot-gitlab/chart@dcf7b44bf41e55a4f48c5421d3123d4e0180f537) by @michbeck100. See merge request dependabot-gitlab/chart!174

### 📦 Dependency updates (2 changes)

- [Update app version to v1.0.0-alpha.4](dependabot-gitlab/chart@9f36c10991386d5410ca3099a61f2a792267751f) by @dependabot-bot. See merge request dependabot-gitlab/chart!177
- [Update Helm release mongodb to ~13.12.0](dependabot-gitlab/chart@a927484ac542f1cb21366a87df493206aa1a1d93) by @dependabot-bot. See merge request dependabot-gitlab/chart!175

### 🛠️ Chore (1 change)

- [Support custom tasks in runner template](dependabot-gitlab/chart@abf99ca40bd7797498f1f423a142f4a670b5502d) by @andrcuns. See merge request dependabot-gitlab/chart!176

## 1.2.0 (2023-05-06)

### 🐞 Bug Fixes (2 changes)

- [Correctly set migration job name if mongodb or service account creation is enabled](dependabot-gitlab/chart@b938267d8f0a51d9a18a640156bf0f668c01b11e) by @andrcuns. See merge request dependabot-gitlab/chart!168
- [Do not use pre hooks if service account has to be created](dependabot-gitlab/chart@377c62e999f5af6a563acb9fe26ce21cf4c858ee) by @andrcuns. See merge request dependabot-gitlab/chart!166

### 📦 Dependency updates (3 changes)

- [Update app version to v1.0.0-alpha.3](dependabot-gitlab/chart@14b90dc62c5be92e3302542f0e28996dfcb7ff89) by @dependabot-bot. See merge request dependabot-gitlab/chart!173
- [Update Helm release redis to ~17.10.0](dependabot-gitlab/chart@b6d7d0c09aff1e91cddd05f5932ae1a851451ef5) by @dependabot-bot. See merge request dependabot-gitlab/chart!169
- [Update Helm release mongodb to ~13.10.0](dependabot-gitlab/chart@00bde916099178ee5cfee5a5745535405b5c0824) by @dependabot-bot. See merge request dependabot-gitlab/chart!170

### 🔧 CI changes (3 changes)

- [Use mr environment variables for repo setup of update-docs job](dependabot-gitlab/chart@0176d2cca321304ec73a18a78954de3b1e898014) by @andrcuns. See merge request dependabot-gitlab/chart!172
- [Update logging helper methods](dependabot-gitlab/chart@6cc37b80d8c00e382e14aa8e93e3fba0524af9f0) by @andrcuns. See merge request dependabot-gitlab/chart!171
- [Add documentation update job](dependabot-gitlab/chart@de6f9cb94c2afed83c0cf1ee62a4d54aea2f89db) by @andrcuns. See merge request dependabot-gitlab/chart!171

## 1.1.0 (2023-04-05)

### 🐞 Bug Fixes (1 change)

- [Fix service account name when set to not being created](dependabot-gitlab/chart@73e1d63dbd6fedff7be42c016cd89700c5796e4a) by @andrcuns. See merge request dependabot-gitlab/chart!160

### 📦 Dependency updates (1 change)

- [Update app version to v1.0.0-alpha.2](dependabot-gitlab/chart@3a516498a003eeb4a469bbfdd51bfd89c5734497) by @dependabot-bot. See merge request dependabot-gitlab/chart!165

### 🔧 CI changes (2 changes)

- [Update kind, helm and kubectl versions](dependabot-gitlab/chart@4577a650f778e05a3619522d4093e7bbd4beb959) by @andrcuns. See merge request dependabot-gitlab/chart!161
- [Extract docker version to environment variable](dependabot-gitlab/chart@f79b75e2e98e179ca87cef6bda0ed391bc4da478) by @andrcuns. See merge request dependabot-gitlab/chart!159

## 1.0.0 (2023-03-23)

### 🚀 Features (2 changes)

- [Add max runtime parameter for updater job](dependabot-gitlab/chart@89260be2b4bfd8ff5789caf683aa0787b2acba3b) by @andrcuns. See merge request dependabot-gitlab/chart!154
- [[BREAKING] Add support for per-ecosystem docker images](dependabot-gitlab/chart@71a11b6d8696d743397236f51436437492c8db55) by @andrcuns. See merge request dependabot-gitlab/chart!152

### 🔬 Improvements (1 change)

- [Add fully configurable updater image pattern](dependabot-gitlab/chart@c0f85f429e0304b1d4c243169ff28612e3f7bb95) by @andrcuns. See merge request dependabot-gitlab/chart!157

### 🐞 Bug Fixes (4 changes)

- [Use came case for image pattern value](dependabot-gitlab/chart@7066e848ac3c7825103c28ee7fe3eb1c5ff19a68) by @andrcuns.
- [Remove slash from updater image naming](dependabot-gitlab/chart@b7131f50bb11a40a7f251a87a245964bbfe9ea73) by @andrcuns. See merge request dependabot-gitlab/chart!156
- [Remove deployment annotations from updater values](dependabot-gitlab/chart@650a523fe85ef2662f6caa21e42f2234c759c88f) by @andrcuns. See merge request dependabot-gitlab/chart!153
- [[BREAKING] Fix typo in helm values](dependabot-gitlab/chart@b8d2643d01e258d6ecc4ab7d5aaf1b95387e05d4) by @petrvyhnalek. See merge request dependabot-gitlab/chart!130

### 📦 Dependency updates (4 changes)

- [Update app version to v1.0.0-alpha.1](dependabot-gitlab/chart@8e0c99e949139a6fbee548b8c536d023025f2032) by @dependabot-bot.
- [Update Helm release redis to ~17.9.0](dependabot-gitlab/chart@dc54a9da5ef9a8d288c37cf84cc39ad6303d0c4b) by @dependabot-bot. See merge request dependabot-gitlab/chart!155
- [Update app version to v0.36.0](dependabot-gitlab/chart@6f40dfd2d8eaae2c101b3a8d61689286715f6bf0) by @dependabot-bot.
- [Update Helm release mongodb to ~13.9.0](dependabot-gitlab/chart@b9dca62ade76844872c3937b2ea9630a0d310cc0) by @dependabot-bot. See merge request dependabot-gitlab/chart!151

## 0.21.0 (2023-03-03)

### 🚀 Features (1 change)

- [Allow configuration of secret key for mongodb password](dependabot-gitlab/chart@825fe04fd3a59c06efedba007060c29c990e2db2) by @ragatzino. See merge request dependabot-gitlab/chart!149

### 🔬 Improvements (1 change)

- [♻️ Separate annotations between the web service and the metrics service](dependabot-gitlab/chart@c5acd10ca29751793db9cfb34f366e6348c5ba5d) by @christophe-olivet. See merge request dependabot-gitlab/chart!150

### 📦 Dependency updates (9 changes)

- [Update app version to v0.35.1](dependabot-gitlab/chart@41e972491dea9a5408f7f0bb7d036fa14c8be8b0) by @dependabot-bot.
- [Update app version to v0.35.0](dependabot-gitlab/chart@2c05ee34f7bab6e9cb316c7c20955c5b766eeb94) by @dependabot-bot.
- [Update Helm release redis to ~17.8.0](dependabot-gitlab/chart@f9db633965d095fb5ace259673e41153b4123153) by @dependabot-bot. See merge request dependabot-gitlab/chart!147
- [Update Helm release mongodb to ~13.8.0](dependabot-gitlab/chart@52061937d7f425a10d9ebc08b18d6979c0a7528d) by @dependabot-bot. See merge request dependabot-gitlab/chart!148
- [Update Helm release mongodb to ~13.7.0](dependabot-gitlab/chart@cc36cddd86bfa5e45427a1070f938e9193cc8c5d) by @dependabot-bot. See merge request dependabot-gitlab/chart!146
- [Update Helm release redis to ~17.7.0](dependabot-gitlab/chart@4a8ff2903f53102c25cc767f2aa3a9d28db19330) by @dependabot-bot. See merge request dependabot-gitlab/chart!142
- [Update app version to v0.34.0](dependabot-gitlab/chart@b046d0a835df4a23c5c6d957be1784912b4e9dc4) by @dependabot-bot.
- [Update app version to v0.33.1](dependabot-gitlab/chart@2461b0dfc53d9e75043470fbfdfd9a30f9d8b895) by @dependabot-bot.
- [Update app version to v0.33.0](dependabot-gitlab/chart@f726d8c09e67b8f9fe63118d99d0fda937abd100) by @dependabot-bot.

### 🔧 CI changes (2 changes)

- [Update docker Docker tag to v23.0.1](dependabot-gitlab/chart@70dab95d54aecf3e9ab2e046555d705064958da2) by @dependabot-bot. See merge request dependabot-gitlab/chart!145
- [Update docker Docker tag to v23](dependabot-gitlab/chart@9c4b3261563fa298b2d643282af9d0a440e182d9) by @dependabot-bot. See merge request dependabot-gitlab/chart!144

## 0.20.0 (2023-01-09)

### 🚀 Features (1 change)

- [Add authentication support](dependabot-gitlab/chart@24d8632f4bd9fc345cb23091ee7221b3b545a94f) by @andrcuns. See merge request dependabot-gitlab/chart!138

### 📦 Dependency updates (3 changes)

- [Update Helm release redis to ~17.4.0](dependabot-gitlab/chart@68eded11c60431fed3efe7fcf6f990fef6ac8d2f) by @dependabot-bot. See merge request dependabot-gitlab/chart!139
- [Update app version to v0.32.0](dependabot-gitlab/chart@13cede80419e1c6bfe8f4d4a1a052f0e037671b7) by @dependabot-bot.
- [Update Helm release mongodb to ~13.6.0](dependabot-gitlab/chart@b976b2e92eb76de977f564df6e00f4f0d3cf4aac) by @dependabot-bot. See merge request dependabot-gitlab/chart!136

### 🔧 CI changes (2 changes)

- [Temporarily allow upgrade jobs to fail](dependabot-gitlab/chart@2f39f6d53e2dc40720d543632e14fb22957a7025) by @andrcuns. See merge request dependabot-gitlab/chart!141
- [Update docker Docker tag to v20.10.22](dependabot-gitlab/chart@2189848b79e406c84aaa278f82910b47ddc8a970) by @dependabot-bot. See merge request dependabot-gitlab/chart!137

## 0.19.0 (2022-12-03)

### 🔬 Improvements (1 change)

- [Add option to provide custom ingress class name](dependabot-gitlab/chart@43635eef327b4afc1cbca1a466590821a15ee10f) by @andrcuns. See merge request dependabot-gitlab/chart!135

## 0.18.0 (2022-12-02)

### 🐞 Bug Fixes (1 change)

- [Fix failure to fetch redis and mongodb secret name](dependabot-gitlab/chart@847dc4e4c33bcf53499c6b13f8d854c69b46de69) by @andrcuns. See merge request dependabot-gitlab/chart!134

### 📦 Dependency updates (3 changes)

- [Update Helm release mongodb to ~13.5.0](dependabot-gitlab/chart@b2e3c004b2c96120a2dbd4d0a9be84de3e1ff3e0) by @dependabot-bot. See merge request dependabot-gitlab/chart!133
- [Update app version to v0.31.1](dependabot-gitlab/chart@ee9dad2ffe26c6e899923122dc807e5dda8a56a7) by @dependabot-bot.
- [Update app version to v0.31.0](dependabot-gitlab/chart@510ba98ae086e648952682cd81d0f602878689e6) by @dependabot-bot.

### 🔧 CI changes (2 changes)

- [Validate templates with different kubernetes versions](dependabot-gitlab/chart@5397ff6cdcdaefb3bc9d65c5dce80807bdbf0b26) by @andrcuns. See merge request dependabot-gitlab/chart!134
- [Replace kubeval with kubeconform](dependabot-gitlab/chart@b07f8f86e6375628be769ebd16ce66bf97ddd3d8) by @andrcuns. See merge request dependabot-gitlab/chart!134

## 0.17.0 (2022-11-08)

### 🚀 Features (1 change)

- [Add hostAliases for worker](dependabot-gitlab/chart@a87aae9da7def0cd59bae6df579078eb747f31e1) by @Tripletri. See merge request dependabot-gitlab/chart!132

## 0.16.0 (2022-11-06)

### 📦 Dependency updates (3 changes)

- [Update app version to v0.30.0](dependabot-gitlab/chart@c6b4d58cd9d92ac93c194633259ce9ba5a544f9e) by @dependabot-bot.
- [Update Helm release mongodb to ~13.4.0](dependabot-gitlab/chart@0f78afe3228d91ef52793c293986fd01c9456531) by @dependabot-bot. See merge request dependabot-gitlab/chart!129
- [Update Helm release mongodb to ~13.3.0](dependabot-gitlab/chart@9fcb9c5eba58988e20fc832380753c2c1c6b57a9) by @dependabot-bot. See merge request dependabot-gitlab/chart!127

### 🔧 CI changes (2 changes)

- [Add parametrized large runner tag](dependabot-gitlab/chart@bbf47c0a55fdea84cf9931cca7439e4f0f299c8c) by @acunskis. See merge request dependabot-gitlab/chart!131
- [Update docker Docker tag to v20.10.21](dependabot-gitlab/chart@22b6fd6177888cc0f67549193999d44f479e5941) by @dependabot-bot. See merge request dependabot-gitlab/chart!128

## 0.15.0 (2022-10-23)

### 🚀 Features (2 changes)

- [Add ability to use existing secret for base configuration](dependabot-gitlab/chart@876de445e6f614d647cd8a7c4d4f841c35a5c4a6) by @andrcuns. See merge request dependabot-gitlab/chart!125
- [Automatic registration ignore pattern option support](dependabot-gitlab/chart@c2c05b787e15a14957e7c237b19bb2934ba487dc) by @andrcuns. See merge request dependabot-gitlab/chart!121

### 🔬 Improvements (1 change)

- [Add option to configure worker concurrency](dependabot-gitlab/chart@f49ba880def19ecbb8c0ef2dadc3a32324689321) by @andrcuns. See merge request dependabot-gitlab/chart!122

### 📦 Dependency updates (2 changes)

- [Update app version to v0.29.0](dependabot-gitlab/chart@ba5229d348e16d79f2ab4829a44b58e9cd577275) by @dependabot-bot.
- [Update app version to v0.28.1](dependabot-gitlab/chart@b90dede681b134be947b366937752a7bba998a78) by @dependabot-bot.

### 🛠️ Chore (1 change)

- [Update project mock for install test](dependabot-gitlab/chart@76472798596aa83fe7a6ea94b67d7322075ec202) by @andrcuns. See merge request dependabot-gitlab/chart!119

### 🔧 CI changes (2 changes)

- [Update docker Docker tag to v20.10.20](dependabot-gitlab/chart@34f249aa71a22901afe185454eb9e3997cec962c) by @dependabot-bot. See merge request dependabot-gitlab/chart!124
- [Update docker Docker tag to v20.10.19](dependabot-gitlab/chart@0efc87519d222114e825dd7174e5242f4306bcb7) by @dependabot-bot. See merge request dependabot-gitlab/chart!120

## 0.14.0 (2022-10-08)

### 🔬 Improvements (2 changes)

- [Add ttl configuration for update run data](dependabot-gitlab/chart@cc334e1de90273171aace28d1f4e1d5bf62d5280) by @andrcuns. See merge request dependabot-gitlab/chart!116
- [Add configurable options for migration job and project registration job](dependabot-gitlab/chart@81ee996000aef21d49ed0f198830117070a19f87) by @andrcuns. See merge request dependabot-gitlab/chart!114

### 🐞 Bug Fixes (1 change)

- [Make sure expiry value is not converted to scientific format](dependabot-gitlab/chart@2a7365018b8ea9f7955af3ab8cee12b33a353203) by @andrcuns. See merge request dependabot-gitlab/chart!117

### 📦 Dependency updates (1 change)

- [Update app version to v0.28.0](dependabot-gitlab/chart@2f17956d8474636c8577b4f9c8bcd3e9905f9d83) by @dependabot-bot.

### 🛠️ Chore (1 change)

- [Do not set uniq migration job name when helm hooks is used](dependabot-gitlab/chart@9c3313e59f4a4c74111a911febd8a158811ce8ea) by @andrcuns. See merge request dependabot-gitlab/chart!115

## 0.13.3 (2022-10-05)

### 📦 Dependency updates (2 changes)

- [Update app version to v0.27.5](dependabot-gitlab/chart@397f6623a9366734b53a56fc64f7bec1e47b4b4d) by @dependabot-bot.
- [Update app version to v0.27.4](dependabot-gitlab/chart@6c750e3de61c7aaf6039a98e54dd7b9384455368) by @dependabot-bot.

## 0.13.2 (2022-10-02)

### 📦 Dependency updates (4 changes)

- [Update app version to v0.27.3](dependabot-gitlab/chart@1fa4b0acd570f1de863169772960d1c27b8d3d01) by @dependabot-bot.
- [Update app version to v0.27.2](dependabot-gitlab/chart@cae9a57089f8ed1d8deab3a2b38c938d638a9860) by @dependabot-bot.
- [Update Helm release redis to ~17.3.0](dependabot-gitlab/chart@21688aabc4427b6d833849f7b8898de0cf3c6571) by @dependabot-bot. See merge request dependabot-gitlab/chart!113
- [Update app version to v0.27.1](dependabot-gitlab/chart@362cee04ef547795cab07dece4dd0cabf6e89924) by @dependabot-bot.

## 0.13.1 (2022-09-24)

### 📦 Dependency updates (1 change)

- [Update Helm release redis to ~17.2.0](dependabot-gitlab/chart@c720bd1c564bf3b5d7477714b0b1846256c0f875) by @dependabot-bot. See merge request dependabot-gitlab/chart!112

## 0.13.0 (2022-09-23)

### 🐞 Bug Fixes (1 change)

- [Fix extra volumes for worker](dependabot-gitlab/chart@c9a40b37c5b58c1ecacdf78924e0aeed33ba6d8b) by @WanzenBug. See merge request dependabot-gitlab/chart!110

### 📦 Dependency updates (1 change)

- [Update app version to v0.27.0](dependabot-gitlab/chart@e4ed98723309a14d69d521678e204a5b476170fc) by @dependabot-bot.

### 🔧 CI changes (1 change)

- [Use large runner for install test jobs](dependabot-gitlab/chart@933dec8bc5a039c78aa0e22a4eb8d388efdad3d6) by @andrcuns. See merge request dependabot-gitlab/chart!111

## 0.12.0 (2022-09-17)

### 🔬 Improvements (1 change)

- [Add resource definitions for init containers](dependabot-gitlab/chart@076254085b5ac36d564b9ab550c2bfe7e94cd766) by @Tripletri. See merge request dependabot-gitlab/chart!108

### 📦 Dependency updates (2 changes)

- [Update app version to v0.26.1](dependabot-gitlab/chart@66adc86ab5b2d27effd58046495cdb0a43ebb33c) by @dependabot-bot.
- [Update app version to v0.26.0](dependabot-gitlab/chart@04ff7ea714e9999e7b25da27b7335a1fe383dfb7) by @dependabot-bot.

### 🔧 CI changes (1 change)

- [Update docker Docker tag to v20.10.18](dependabot-gitlab/chart@2ebff061c74730405d6c2632bb025285f52a0254) by @dependabot-bot. See merge request dependabot-gitlab/chart!109

## 0.11.2 (2022-08-30)

### 🔬 Improvements (1 change)

- [Add component label to migration-job](dependabot-gitlab/chart@1560d83f81edb4eb00fd04aa0fbc7415269954e6) by @solidnerd. See merge request dependabot-gitlab/chart!107

## 0.11.1 (2022-08-29)

### 📦 Dependency updates (2 changes)

- [Update app version to v0.25.2](dependabot-gitlab/chart@116c85de65a5226f1a24ef4a9509631203499e34) by @dependabot-bot.
- [Update app version to v0.25.1](dependabot-gitlab/chart@51b5bb7184b2bde6fa465501f495556629169962) by @dependabot-bot.

## 0.11.0 (2022-08-26)

### 📦 Dependency updates (4 changes)

- [Update Helm release redis to ~17.1.0](dependabot-gitlab/chart@7d72b4ce42f3c5382a81fc3db99ab84d44fe75de) by @dependabot-bot. See merge request dependabot-gitlab/chart!106
- [Update Helm release mongodb to ~13.1.0](dependabot-gitlab/chart@4240a6304e95962d2c7f9277194025e861dda680) by @dependabot-bot. See merge request dependabot-gitlab/chart!105
- [Update Helm release common to ~1.17.0](dependabot-gitlab/chart@48d40464938b14d6267f9d024f53f294c69f43b5) by @dependabot-bot. See merge request dependabot-gitlab/chart!103
- [Update app version to v0.25.0](dependabot-gitlab/chart@35345aa7ccb69f083eb89c3e2aec935261ccdc59) by @dependabot-bot.

## 0.10.0 (2022-08-09)

### 📦 Dependency updates (1 change)

- [Update Helm release mongodb to v13](dependabot-gitlab/chart@2c61d0529b2ecdb94846ec539a7aad5b48c1fb67) by @dependabot-bot. See merge request dependabot-gitlab/chart!102

## 0.9.1 (2022-07-30)

### 🐞 Bug Fixes (1 change)

- [Update charts/dependabot-gitlab/templates/migration-job.yaml](dependabot-gitlab/chart@a6bf10549b42a4f12e45c061ccfe97097c3aa683) by @balazs92117.

### 📦 Dependency updates (1 change)

- [Update app version to v0.24.0](dependabot-gitlab/chart@5127ec6932f0d01c9411e1447d8a72c7fe22f870) by @dependabot-bot.

## 0.9.0 (2022-07-15)

### 📦 Dependency updates (3 changes)

- [Update app version to v0.23.0](dependabot-gitlab/chart@a98047b4b3cfffdbb7a7dd23102b04c071329611) by @dependabot-bot.
- [Update Helm release redis to v17](dependabot-gitlab/chart@6884c274cf50fd1c650035fa383bff414b2014a0) by @dependabot-bot. See merge request dependabot-gitlab/chart!99
- [Update app version to v0.22.4](dependabot-gitlab/chart@7ca91def7a9e3d92269a3b3c2dc561659f517926) by @dependabot-bot.

## 0.8.2 (2022-06-26)

### 📦 Dependency updates (2 changes)

- [Update app version to v0.22.3](dependabot-gitlab/chart@02fdfcdec98efe3827a87cb9229be7026e704a8b) by @dependabot-bot.
- [Update Helm release redis to ~16.13.0](dependabot-gitlab/chart@99e0c2aed23f632a1e58372cc68b8a059ad177dc) by @dependabot-bot. See merge request dependabot-gitlab/chart!98

## 0.8.1 (2022-06-24)

### 📦 Dependency updates (2 changes)

- [Update app version to v0.22.2](dependabot-gitlab/chart@7767d8bea10440288e1415f3d93eb82bcab1708d) by @dependabot-bot.
- [Update app version to v0.22.1](dependabot-gitlab/chart@064643421ef0ea6d010dc164686553bdf977fa72) by @dependabot-bot.

## 0.8.0 (2022-06-18)

### 🚀 Features (1 change)

- [Dependabot base configuration support](dependabot-gitlab/chart@12a98e2bd52a2f17f78b87c3e489a216f9e52ea0) by @andrcuns. See merge request dependabot-gitlab/chart!97

### 📦 Dependency updates (3 changes)

- [Update app version to v0.22.0](dependabot-gitlab/chart@33db997713698385a81355fa51beb563924eb6c5) by @dependabot-bot.
- [Update app version to v0.21.1](dependabot-gitlab/chart@a73f41754e3da7c78aa45403c69862eaf2b3427d) by @dependabot-bot.
- [Update Helm release redis to ~16.12.0](dependabot-gitlab/chart@7ddffa38852c67f721114e43d09317f522a8647e) by @dependabot-bot. See merge request dependabot-gitlab/chart!96

### 🔧 CI changes (2 changes)

- [Update runner image for install jobs](dependabot-gitlab/chart@d1cc71408d9fed3c26dba42629201dae78248d9e) by @andrcuns.
- [Update dependency docker to v20.10.17](dependabot-gitlab/chart@21e6254cda2e307c4a98468c22c3f0c9be6c1701) by @dependabot-bot. See merge request dependabot-gitlab/chart!95

## 0.7.7 (2022-06-06)

### 📦 Dependency updates (3 changes)

- [Update app version to v0.21.0](dependabot-gitlab/chart@51b24a3e350b3abb85c3fd88167d9f47327971d7) by @dependabot-bot.
- [Update Helm release redis to ~16.11.0](dependabot-gitlab/chart@8ecf057258cb6281cf2367c524ba3db91c0c9a5b) by @dependabot-bot. See merge request dependabot-gitlab/chart!94
- [Update Helm release common to ~1.16.0](dependabot-gitlab/chart@88ab465cccdd9188cfcf3f3961eac139403af53c) by @dependabot-bot. See merge request dependabot-gitlab/chart!93

## 0.7.6 (2022-06-03)

### 📦 Dependency updates (1 change)

- [Update Helm release common to ~1.15.0](dependabot-gitlab/chart@133c2c8a99a155f91653e1ad7c5c3b6ef462049f) by @dependabot-bot. See merge request dependabot-gitlab/chart!92

## 0.7.5 (2022-05-29)

### 📦 Dependency updates (1 change)

- [Update Helm release redis to ~16.10.0](dependabot-gitlab/chart@69ad5fd0fa23cf42bc8c3a475a4db0beaf6250d9) by @dependabot-bot. See merge request dependabot-gitlab/chart!88

### 🛠️ Chore (1 change)

- [Migrate to main branch](dependabot-gitlab/chart@a989815c8e6c72868e722e8a0c33117b974330e8) by @andrcuns. See merge request dependabot-gitlab/chart!91

### 🔧 CI changes (2 changes)

- [Update ci image](dependabot-gitlab/chart@d9f6c1012c704bf7837a3c7498150532e496c925) by @andrcuns. See merge request dependabot-gitlab/chart!90
- [Improve chart caching and installation in ci jobs](dependabot-gitlab/chart@8039dfc24868784eac549c079b8bf6ff71e503d9) by @andrcuns. See merge request dependabot-gitlab/chart!89

## 0.7.4 (2022-05-25)

### 📦 Dependency updates (1 change)

- [Update app version to v0.20.2](dependabot-gitlab/chart@56f1f95cb3659145ef1709ece011a8014bcc9f83) by @dependabot-bot.

## 0.7.3 (2022-05-17)

### 🚀 Features (1 change)

- [Add hability to add pod label](dependabot-gitlab/chart@33154ae8b895bfe3f390f132054a17e56e083436) by @Donatien26. See merge request dependabot-gitlab/chart!87

### 📦 Dependency updates (3 changes)

- [Update Helm release common to ~1.14.0](dependabot-gitlab/chart@da46a1e0d9a10bcf78810ffd0865b93c4116663a) by @dependabot-bot. See merge request dependabot-gitlab/chart!86
- [Update app version to v0.20.1](dependabot-gitlab/chart@f78476d89a8ac2085c32764b48fb65a273e0e613) by @dependabot-bot.
- [Update app version to v0.20.0](dependabot-gitlab/chart@ea93c2b7f32d748d85e0aaed890c10f3090ba819) by @dependabot-bot.

### 🔧 CI changes (1 change)

- [Update dependency docker to v20.10.16](dependabot-gitlab/chart@054cdce9a2970d7a2e051bf02bf4cd02beea97ea) by @dependabot-bot. See merge request dependabot-gitlab/chart!85

## 0.7.2 (2022-05-08)

### 📦 Dependency updates (3 changes)

- [Update app version to v0.19.2](dependabot-gitlab/chart@14a8dd728039e5dd9a3695093bad25396bed81bc) by @dependabot-bot.
- [Update Helm release redis to ~16.9.0](dependabot-gitlab/chart@29b1fc0eed0c4185a1ba2c495fdeb41675cbd102) by @dependabot-bot. See merge request dependabot-gitlab/chart!82
- [Update Helm release mongodb to ~12.1.0](dependabot-gitlab/chart@2a32c544a3d9eea7921ce8ce287292060617bdee) by @dependabot-bot. See merge request dependabot-gitlab/chart!81

### 🔧 CI changes (3 changes)

- [Fix certificate for kind cluster](dependabot-gitlab/chart@1415aaa9776a382dd24bc9f3690f83b61a9b5c11) by @andrcuns.
- [Update ingress installation](dependabot-gitlab/chart@2be27dd7b0fd951c76a7a2ca367a9a6902da3f36) by @andrcuns. See merge request dependabot-gitlab/chart!83
- [Update dependency docker to v20.10.15](dependabot-gitlab/chart@d27d0e2d66158a36a5d493a6accbc3411d2df6ce) by @dependabot-bot. See merge request dependabot-gitlab/chart!80

## 0.7.1 (2022-05-06)

### 🐞 Bug Fixes (1 change)

- [Fix nodeselector for web](dependabot-gitlab/chart@49a79a03db726d5eb93d971d31170cc256331080) by @hoerup. See merge request dependabot-gitlab/chart!79

### 📦 Dependency updates (1 change)

- [Update app version to v0.19.1](dependabot-gitlab/chart@3e31e72c1851396d8ac89e8e22320ec193fd3909) by @dependabot-bot.

## 0.7.0 (2022-04-30)

### 📦 Dependency updates (1 change)

- [Update Helm release mongodb to v12](dependabot-gitlab/chart@4abbf455578d3d340bce90bb0d7194c9baa88704) by @dependabot-bot. See merge request dependabot-gitlab/chart!78

## 0.6.0 (2022-04-30)

### 🚀 Features (1 change)

- [Configurable ignored sentry error list](dependabot-gitlab/chart@e2d31d8700aacb275ad0fb44ebb29c0ac8f9e988) by @andrcuns. See merge request dependabot-gitlab/chart!75

### 🔬 Improvements (1 change)

- [Add project hook creation option](dependabot-gitlab/chart@9a398e5505cbd95cf5b73e1eca85924ac77cb9c1) by @sebbrandt87. See merge request dependabot-gitlab/chart!77

### 🔧 CI changes (1 change)

- [Update dependency docker to v20.10.14](dependabot-gitlab/chart@3f538f175e1b4601b9e1684a0c959c339a26a2ef) by @dependabot-bot. See merge request dependabot-gitlab/chart!71

### 📦 Dependency updates (3 changes)

- [Update app version to v0.19.0](dependabot-gitlab/chart@afd844ca78be2a02ea506366c3e9964dad60f173) by @dependabot-bot.
- [Update Helm release redis to ~16.8.0](dependabot-gitlab/chart@9e667d1f0cff1a15da57312d1286475f0095410b) by @dependabot-bot. See merge request dependabot-gitlab/chart!74
- [Update Helm release common to ~1.13.0](dependabot-gitlab/chart@2bcfea9a136dae3371f7c938d2e9d41f5025185a) by @dependabot-bot. See merge request dependabot-gitlab/chart!72

## 0.5.10 (2022-04-22)

### 📦 Dependency updates (1 change)

- [Update app version to v0.18.0](dependabot-gitlab/chart@0cfbf724e4853bb01536200460688700ebd6a6eb) by @dependabot-bot.

## 0.5.9 (2022-04-09)

### 📦 Dependency updates (3 changes)

- [Update app version to v0.17.2](dependabot-gitlab/chart@55ac11552bc085f8647161ccfcddde234f2cc9c5) by @dependabot-bot.
- [Update app version to v0.17.1](dependabot-gitlab/chart@ed2a343cfbe856c8f3a1c5585ae1bc8053d577a0) by @dependabot-bot.
- [Update app version to v0.17.0](dependabot-gitlab/chart@9a16b0b041baf7c28b40d3385baa36581c136a07) by @dependabot-bot.

## 0.5.8 (2022-03-18)

### 🛠️ Chore (1 change)

- [Mount registries secrets in to migration job](dependabot-gitlab/chart@32de9f9dae9f500114daeba1620c6efc636ef533) by @andrcuns. See merge request dependabot-gitlab/chart!69

## 0.5.7 (2022-03-16)

### 📦 Dependency updates (1 change)

- [Update app version to v0.16.0](dependabot-gitlab/chart@ff6d65ac1d65a222c40b54f9f11f34a02df23c3c) by @dependabot-bot.

## 0.5.6 (2022-03-13)

### 🚀 Features (1 change)

- [Add colorized logs option](dependabot-gitlab/chart@f442644ac0b3e6173be272963b5fba2c03159553) by @andrcuns. See merge request dependabot-gitlab/chart!68

## 0.5.5 (2022-03-12)

### 📦 Dependency updates (2 changes)

- [Update app version to v0.15.3](dependabot-gitlab/chart@03d47fe56c0c8ee5b80a505f6361ddebdb04708d) by @dependabot-bot.
- [Update app version to v0.15.2](dependabot-gitlab/chart@88f9a1a90f90bc58035a6e3e5bb5585190a1696a) by @dependabot-bot.

## 0.5.4 (2022-02-21)

### 📦 Dependency updates (1 change)

- [Update app version to v0.15.1](dependabot-gitlab/chart@f3b0642b13301876a8dc181db3f8b0bd299cfaf4) by @dependabot-bot.

## 0.5.3 (2022-02-21)

### 📦 Dependency updates (1 change)

- [Update app version to v0.15.0](dependabot-gitlab/chart@31a4efcf981ecbf17ecf7c1cf835c2fff13f53e5) by @dependabot-bot.

## 0.5.2 (2022-02-16)

### 📦 Dependency updates (1 change)

- [Update app version to v0.14.2](dependabot-gitlab/chart@78bac4b10ab7f0da69ce1e56459a27006b8696d0) by @dependabot-bot.

## 0.5.1 (2022-02-11)

### 📦 Dependency updates (1 change)

- [Update app version to v0.14.1](dependabot-gitlab/chart@628982847c509f8197ae12cd2ff256495a5efdda) by @dependabot-bot.

## 0.5.0 (2022-02-10)

### 🐞 Bug Fixes (1 change)

- [[BREAKING] Store secret key base in secrets object](dependabot-gitlab/chart@bdad0dcb04895444c9926fbce374c2967f46f58e) by @andrcuns. See merge request dependabot-gitlab/chart!67

### 📦 Dependency updates (1 change)

- [Update app version to v0.14.0](dependabot-gitlab/chart@a4bc85121e712d49153869ae69110bc54d3224fa) by @dependabot-bot.

## 0.4.1 (2022-02-06)

### 🐞 Bug Fixes (1 change)

- [Add missing registries credentials for project registration job](dependabot-gitlab/chart@105d5bfb66107e5e48196fe6cc2c49fde0b28acb) by @andrcuns. See merge request dependabot-gitlab/chart!66

## 0.4.0 (2022-01-30)

### 🐞 Bug Fixes (1 change)

- [Fix conditions for db and secrets checksums](dependabot-gitlab/chart@24bb9cf2b9e9eb337fb5e3cf2422998791459df8) by @andrcuns. See merge request dependabot-gitlab/chart!61

### 🔧 CI changes (3 changes)

- [Use versioned ci image](dependabot-gitlab/chart@a6c0aa67f861b1d9a81d79bd757af854f43d9242) by @andrcuns. See merge request dependabot-gitlab/chart!65
- [Add ability to override install values for upgrade tests](dependabot-gitlab/chart@c3d990ef583bb54ac526c078bb0efc5174519d19) by @andrcuns. See merge request dependabot-gitlab/chart!64
- [Modify version before upgrade test](dependabot-gitlab/chart@3b9de934e8aabc255503ba0cb3b8ce08fe884bd3) by @andrcuns. See merge request dependabot-gitlab/chart!62

### 📦 Dependency updates (2 changes)

- [Bump mongodb chart version to 11.0.0](dependabot-gitlab/chart@13aaa4acdfad142ef8d48c7145633b648a2c1130) by @andrcuns. See merge request dependabot-gitlab/chart!59
- [Bump redis chart version to 16.2.0](dependabot-gitlab/chart@305860f0b82fd57368d8d91985d290a226519310) by @andrcuns. See merge request dependabot-gitlab/chart!60

## 0.3.3 (2022-01-30)

### 🐞 Bug Fixes (2 changes)

- [Correctly set redis url depending on configuration](dependabot-gitlab/chart@28a31f37fbad8645c1b64736cccdc00130d860ed) by @andrcuns. See merge request dependabot-gitlab/chart!56
- [Use correct attributes for mongodb checksum](dependabot-gitlab/chart@e0778baa6b01789c798c7780bb1931fe58749c74) by @andrcuns. See merge request dependabot-gitlab/chart!54

### 🔧 CI changes (3 changes)

- [Add tests with ingress configuration](dependabot-gitlab/chart@ba65f4a0cc96e4ad1a275d2010293c9c8daa5b1a) by @andrcuns. See merge request dependabot-gitlab/chart!57
- [Use helm lint instead of chart-testing for linting](dependabot-gitlab/chart@68f5cd89b83df6050c2f73924a6483595ac9318d) by @andrcuns. See merge request dependabot-gitlab/chart!55
- [Fetch previous release from git in upgrade test](dependabot-gitlab/chart@4ad960e73439c11b56eb4746f9f4428b652c7cad) by @andrcuns. See merge request dependabot-gitlab/chart!53

## 0.3.2 (2022-01-29)

### 🔬 Improvements (1 change)

- [Do not create empty secrets object when registries credentials are not defined](dependabot-gitlab/chart@828ccf49d72c918b80dc83144f74b2e6602da614) by @andrcuns. See merge request dependabot-gitlab/chart!50

### 🔧 CI changes (2 changes)

- [Add upgrade job](dependabot-gitlab/chart@449b7f39ad0e66e2156a427e31ecf4716e9e562a) by @andrcuns. See merge request dependabot-gitlab/chart!52
- [Add install test with existing secrets config](dependabot-gitlab/chart@4ce79922c8400b74013f84cd2c9a2112d03934f8) by @andrcuns. See merge request dependabot-gitlab/chart!51

## 0.3.1 (2022-01-28)

### 🐞 Bug Fixes (1 change)

- [fix secretKeyRef in web and worker deployment](dependabot-gitlab/chart@b205c229c5d2a24112947dfa8a03b1ab3b3fd948) by @stieglma. See merge request dependabot-gitlab/chart!48

## 0.3.0 (2022-01-26)

### 🚀 Features (2 changes)

- [Add existing secrets option for credentials and registries credentials](dependabot-gitlab/chart@afb30f07e0d1491b3de1d23d8402c73fd4082f16) by @stieglma. See merge request dependabot-gitlab/chart!47
- [Deployment annotations support for web and worker](dependabot-gitlab/chart@d05f1398950cb42396a1faa79e6cfc079b705018) by @radekgrebski. See merge request dependabot-gitlab/chart!46

### 🔬 Improvements (3 changes)

- [[BREAKING] Correctly handle existing secrets for mongodb and redis](dependabot-gitlab/chart@ee990654875cd0337b27bdf70f96da81ae61ea54) by @andrcuns. See merge request dependabot-gitlab/chart!41
- [Add option to provide custom serviceAccount name](dependabot-gitlab/chart@885fc126b0678da255b7cd0cf39da72e39ab294f) by @andrcuns. See merge request dependabot-gitlab/chart!40
- [[BREAKING] Add bitnami common helpers](dependabot-gitlab/chart@c1ddf8f92f71556ad33b46594cc4329c0aad31dc) by @andrcuns. See merge request dependabot-gitlab/chart!39

### 🔧 CI changes (3 changes)

- [Add gitlab mock to test setup](dependabot-gitlab/chart@5feb976c93d875f00ba829ab0f2d213e98ec0a2d) by @andrcuns. See merge request dependabot-gitlab/chart!44
- [Improve kubeval log output](dependabot-gitlab/chart@47ee6ed865b0cf424d9def8c77d3d09aeefac38b) by @andrcuns. See merge request dependabot-gitlab/chart!43
- [Add install test with different values files](dependabot-gitlab/chart@edf2070cd00e3d3a9261065c603ad9b3c5231ada) by @andrcuns. See merge request dependabot-gitlab/chart!42

### 📦 Dependency updates (1 change)

- [Update app version to v0.13.0](dependabot-gitlab/chart@af28f1693da910b6c50b526810bf6c2446f4b236) by @dependabot-bot.

### fix (1 change)

- [Set correct default values for extra volumes and env vars](dependabot-gitlab/chart@e0b1f3ec2e80d16f7cb67399d14fc2b0f93cfbf6) by @andrcuns. See merge request dependabot-gitlab/chart!45

## 0.2.4 (2021-12-21)

### 📦 Dependency updates (1 change)

- [Update app version to v0.12.0](dependabot-gitlab/chart@f3d9525e4870620c846c38476de6f9a3103ef2bc) by @dependabot-bot.

## 0.2.3 (2021-12-18)

### 🐞 Bug Fixes (1 change)

- [Fixes extraEnvVars](dependabot-gitlab/chart@be30d9e0ef95187f7b3beebe7d82e61446bd92e0) by @christophefromparis. See merge request dependabot-gitlab/chart!37

### 🔧 CI changes (1 change)

- [Remove ci image build](dependabot-gitlab/chart@9490b92b6e9c7cb48fd6fd89cd5f7ba6e3e24147) by @andrcuns. See merge request dependabot-gitlab/chart!36

### 📦 Dependency updates (1 change)

- [Update app version to v0.11.0](dependabot-gitlab/chart@0bcf63fa0a5ab3c41455d05a7c43ecb21ba15cfe) by @dependabot-bot.

## 0.2.2 (2021-12-10)

### 🔬 Improvements (1 change)

- [Add configurable secret key base](dependabot-gitlab/chart@b30deb1a1f5f30b7a5688d165ec85dc0a8312387) by @andrcuns. See merge request dependabot-gitlab/chart!35

## 0.2.1 (2021-12-07)

### 🔧 CI changes (3 changes)

- [Update changelog template](dependabot-gitlab/chart@b60beb793d3dc65044fb89c97ac041641b7ed342) by @andrcuns.
- [Fix gitlab release creation](dependabot-gitlab/chart@b6e2853b6ac61b5a39661d511250ffce95f1bded) by @andrcuns.
- [Fix changelog script](dependabot-gitlab/chart@bc531b718488d3f1ab205ae2673971d0b6183dfc) by @andrcuns. See merge request dependabot-gitlab/chart!34

### 📦 Dependency updates (1 change)

- [Update app version to v0.10.11](dependabot-gitlab/chart@8e11a361f54e456a7ee13a60840ac4cd45f8104f) by @dependabot-bot.

## 0.2.0 (2021-11-18)

### 🔧 CI changes (1 change)

- [Remove docker tag from install job](dependabot-gitlab/chart@c09a964c51084e5ceeda03b1e69b67bf54daf777) ([merge request](dependabot-gitlab/chart!28))

### 📦 Dependency updates (2 changes)

- [Bump mongodb chart version](dependabot-gitlab/chart@8c4e504ed226e54c14a5176e5616e18135f92a39) ([merge request](dependabot-gitlab/chart!29))
- [Bump redis chart version](dependabot-gitlab/chart@14963d45e8aaaa08c36cb4bb2d529dbcce46cfef) ([merge request](dependabot-gitlab/chart!27))

## 0.1.3 (2021-11-18)

### 🚀 Features (1 change)

- [Update app version to v0.10.9](dependabot-gitlab/chart@af3942f30ea1c7f04cce38cd4c51e29bca6987b9)

### 🔧 CI changes (1 change)

- [Always build chart](dependabot-gitlab/chart@0099c19fcba4d1d418f2f53e2401c9ae66598b58) ([merge request](dependabot-gitlab/chart!24))

### 🛠️ Chore (1 change)

- [Publish logo in pages](dependabot-gitlab/chart@892da0d186996cb1eb8e56ff9a83d2a2e6044f31) ([merge request](dependabot-gitlab/chart!26))

## 0.1.2 (2021-11-16)

### 🔧 CI changes (2 changes)

- [Package readme in license with chart](dependabot-gitlab/chart@c1d1624556170c40961f40e6a59d26c832fe9ad8) ([merge request](dependabot-gitlab/chart!21))
- [Install grep in CI image](dependabot-gitlab/chart@114ea966b6085729a4e85f2447a0020c52f616a5) ([merge request](dependabot-gitlab/chart!20))

### 🛠️ Chore (2 changes)

- [Fix logo url](dependabot-gitlab/chart@965d68c20bddf959e5c20c22ecc962e6c4537e2b) ([merge request](dependabot-gitlab/chart!23))
- [Add artifacthub repo info](dependabot-gitlab/chart@2565e97eb4ddcdfe86426459099b0549bfdc37b1) ([merge request](dependabot-gitlab/chart!22))

## 0.1.1 (2021-11-16)

### 🐞 Bug Fixes (1 change)

- [Generate tag specific migration job name](dependabot-gitlab/chart@aa94d111306266d4ae0ce7e9e22d5ced5b497d84) ([merge request](dependabot-gitlab/chart!17))

### 🔧 CI changes (2 changes)

- [Generate changelog for releases](dependabot-gitlab/chart@9b391ee0238a029584981cebaf164a454fe36e0e) ([merge request](dependabot-gitlab/chart!15))
- [Do not publish chart if tests failed](dependabot-gitlab/chart@daed1ccbc9ef790375c0647f848f580a9a3a863b) ([merge request](dependabot-gitlab/chart!14))

### 🛠️ Chore (1 change)

- [Shorten job names](dependabot-gitlab/chart@9e4b7de92e12c95cd22dbc1396016fb90c49d453) ([merge request](dependabot-gitlab/chart!19))

### 📄 Documentation updates (1 change)

- [Update value documentation and default values](dependabot-gitlab/chart@4ee7c24998f8435665314a907b9231918f09148d) ([merge request](dependabot-gitlab/chart!16))

## 0.1.0 (2021-11-14)

### 🛠️ Chore (1 change)

- [Update release tag format and fix release script](dependabot-gitlab/chart@d740a28ee1698189511a9c3293820fc53baf08e5) ([merge request](dependabot-gitlab/chart!13))

### 📄 Documentation updates (1 change)

- [Update chart url in documentation](dependabot-gitlab/chart@94478e14d2fc75a2c1bb17b8aee8d8d9b110b1b9) ([merge request](dependabot-gitlab/chart!12))

### CI (1 change)

- [Add chart release jobs and utils](dependabot-gitlab/chart@ea1a45999523d3cd9bedb53ddbf2290e022072bd) ([merge request](dependabot-gitlab/chart!11))
