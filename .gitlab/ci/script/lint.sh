#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

chart_dir="charts/dependabot-gitlab"

update_dependencies

log_with_header "Lint chart template files"
for yaml in .gitlab/ci/kube/values/*.yaml; do
  info "Validating $chart_dir with $yaml" "-"

  helm lint $chart_dir --strict --values $yaml
done
