#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

channel="${1}"

if [ "${channel}" != "stable" ]; then
  chart_tgz="dependabot-gitlab-dev.tgz"
  mv dependabot-gitlab-*.tgz "$chart_tgz"
else
  chart_tgz="$(echo dependabot-gitlab-*.tgz)"
fi

version="$(echo $chart_tgz | sed -E 's/dependabot-gitlab-(.*)\.tgz/\1/')"
url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/chart/${version}/${chart_tgz}"

log_with_header "Publish helm chart"
curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
  --upload-file "$chart_tgz" \
  "$url"
