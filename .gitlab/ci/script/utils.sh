#!/bin/bash

function success() {
  echo -e "\033[1;32m$1\033[0m"
}

function warn() {
  echo -e "\033[1;33m$1\033[0m"
}

function info() {
  echo -e "\033[1;34m$1\033[0m"
}

function title() {
  echo -e "\033[1;35m** $1 **\033[0m"
}

function log_with_header() {
  length=$(echo "$1" | awk '{print length}')
  delimiter=$(head -c $length </dev/zero | tr '\0' "${2:-=}")

  echo -e "\033[1;35m$delimiter\033[0m"
  echo -e "\033[1;35m$1\033[0m"
  echo -e "\033[1;35m$delimiter\033[0m"
}

function chart_version() {
  helm local-chart-version get -c "${CI_PROJECT_DIR}/${CHART_DIR}"
}

function update_dependencies() {
  log_with_header "Update chart dependencies"

  local chart_dir="charts/dependabot-gitlab"

  if [ -d "$chart_dir/charts" ]; then
    success "Cached dependencies exist, skipping ..."
  else
    helm repo add bitnami https://charts.bitnami.com/bitnami
    helm dependency build $chart_dir --skip-refresh
  fi
}

function setup_git() {
  local ref=$1

  title "Checking if GIT_PUSH_TOKEN is present"
  if [ -z "${GIT_PUSH_TOKEN:-}" ]; then
    warn "GIT_PUSH_TOKEN is not present!"
    warn "Please create project access token with write_repository permissions and add GIT_PUSH_TOKEN variable in your GitLab project CI/CD settings"
    exit 1
  else
    success "GIT_PUSH_TOKEN is present"
  fi

  title "Configuring git"

  info "Setting up git user"
  if [ -z "${GITLAB_USER_EMAIL:-}" ]; then
    warn "GITLAB_USER_EMAIL is not present!"
    warn "Using default email 'ci@example.com'"
    warn "You may need to remove valid user validation under 'Repository > Push rules' settings"
    GITLAB_USER_EMAIL="ci@example.com"
  fi
  git config --global user.name "CI"
  git config --global user.email "$GITLAB_USER_EMAIL"
  success "Successfully configured git user"

  info "Setting origin to 'gitlab.com/${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH:-$CI_PROJECT_PATH}'"
  git remote set-url origin "https://git-push-token:${GIT_PUSH_TOKEN}@gitlab.com/${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH:-$CI_PROJECT_PATH}.git"
  success "Successfully set origin"

  info "Fetching origin and checking out branch"
  git fetch && git checkout $ref
  success "Successfully configured git and set current branch to '$ref'"
}
