#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

chart_url="https://dependabot-gitlab.gitlab.io/chart"
version="$(chart_version)"

info "Package helm chart"
cp README.md LICENSE "${CHART_DIR}/"
if [ -d "${CHART_DIR}/charts" ]; then
  helm package "$CHART_DIR"
else
  helm package --dependency-update "$CHART_DIR"
fi

info "Fetch index.yaml from ${chart_url}"
curl -f -o index.yaml "${chart_url}/index.yaml"

info "Update index.yml"
helm repo index . --merge index.yaml --url "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/chart/${version}"
mv index.yaml public/
echo "Updated index.yaml"
