#!/bin/bash

set -eo pipefail

source "$(dirname "$0")/utils.sh"

function helm-install() {
  local command=$1
  local chart=$2
  local values=$3
  local extra_values=$4
  local override_values=".gitlab/ci/kube/override/${VALUES}.yaml"

  if [[ "$command" == "diff upgrade" ]]; then
    local install_args="--allow-unreleased --show-secrets --color --context 5"
  else
    local install_args="--timeout 8m --wait --wait-for-jobs"
  fi

  if [[ "$command" == "install" && "$CI_JOB_NAME" == "upgrade"* ]] && [[ -f "$override_values" ]]; then
    local override_arg="--values ${override_values}"
  fi

  if [[ -n "$extra_values" ]]; then
    local extra_args="--set $extra_values"
  fi

  helm $command "$RELEASE_NAME" "$chart" \
    --set env.gitlabUrl=http://gitlab-smocker.gitlab.svc.cluster.local:8080 \
    --namespace "$NAMESPACE" \
    --values "$values" \
    $install_args \
    $override_arg \
    $extra_args
}

chart_tgz=dependabot-gitlab-*.tgz
values_yml=".gitlab/ci/kube/values/${VALUES}.yaml"
nightly_tag="image.tag=nightly"

if [[ "$CI_JOB_NAME" == "upgrade"* ]]; then
  log_with_header "Run helm upgrade"

  title "Fetch previous release values.yml"
  helm repo add dependabot https://dependabot-gitlab.gitlab.io/chart
  release_version=$(helm search repo dependabot --output json | jq -r ".[-1].version")
  git fetch origin tag "v${release_version}" --no-tags
  git worktree add "release" "v${release_version}"

  title "Install current release"
  helm-install "install" "dependabot/dependabot-gitlab" "release/$values_yml"

  title "Upgrade dependabot release"
  helm-install "diff upgrade" $chart_tgz $values_yml $nightly_tag
  helm-install "upgrade" $chart_tgz $values_yml $nightly_tag
else
  log_with_header "Run helm install"
  helm-install "diff upgrade" $chart_tgz $values_yml $nightly_tag
  helm-install "install" $chart_tgz $values_yml $nightly_tag
fi
