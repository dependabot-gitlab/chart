.cache:
  cache:
    key:
      files:
        - charts/dependabot-gitlab/Chart.lock
    paths:
      - charts/dependabot-gitlab/charts
    policy: pull

.with-helm:
  image: registry.gitlab.com/dependabot-gitlab/ci-images/helm:3.17

.with-helm-docs:
  image: registry.gitlab.com/dependabot-gitlab/ci-images/helmdocs:1.14

.install:
  stage: test
  image: registry.gitlab.com/dependabot-gitlab/ci-images/docker-helm-kubectl-kind:27.4-3.16-1.32-0.25
  services:
    - docker:28.0-dind
  variables:
    DOCKER_HOST: tcp://docker:2375
    NAMESPACE: dependabot
    RELEASE_NAME: dependabot
  parallel:
    matrix:
      - VALUES: [default, secrets, ingress]
  before_script:
    - .gitlab/ci/script/install-kind.sh
    - .gitlab/ci/script/setup-cluster.sh
  script:
    - .gitlab/ci/script/install-app.sh
  after_script:
    - kind export logs "$CI_PROJECT_DIR/kind-logs"
    - echo -e "\e[0Ksection_start:`date +%s`:my_first_section[collapsed=true]\r\e[0KPod logs"
    - .gitlab/ci/script/log-install.sh
    - echo -e "\e[0Ksection_end:`date +%s`:my_first_section\r\e[0K"
  artifacts:
    when: on_failure
    expire_in: 1 day
    paths:
      - kind-logs

# Build stage
.build-helm-chart:
  stage: build
  extends:
    - .cache
    - .with-helm
  script:
    - .gitlab/ci/script/build-chart.sh
  cache:
    policy: pull-push
  artifacts:
    paths:
      - public/index.yaml
      - dependabot-gitlab-*.tgz

# Static analysis stage
.lint-chart:
  stage: static analysis
  extends:
    - .cache
    - .with-helm
  script:
    - .gitlab/ci/script/lint.sh

.lint-docs:
  extends: .with-helm-docs
  stage: static analysis
  script:
    - .gitlab/ci/script/lint-docs.sh

.kubeconform:
  stage: static analysis
  image: registry.gitlab.com/dependabot-gitlab/ci-images/helm-kubeconform:3.17-0.6
  extends: .cache
  parallel:
    matrix:
      - KUBERNETES_VERSION: ['1.24.1', '1.25.1', '1.26.4', '1.27.1', '1.28.6']
  script:
    - .gitlab/ci/script/kubeconform.sh

# Test stage
.install-chart:
  extends: .install

.upgrade-chart:
  extends: .install

# Docs stage
.update-docs:
  extends: .with-helm-docs
  stage: docs
  needs: []
  script:
    - .gitlab/ci/script/update-docs.sh

# Release stage
.publish:
  extends: .with-helm
  stage: release
  script:
    - .gitlab/ci/script/publish-chart.sh "$CHANNEL"
  interruptible: false

.release:
  stage: release
  script:
    - cp artifacthub-repo.yml public/
  pages: true
  artifacts:
    paths:
      - public
  interruptible: false

.changelog:
  stage: release
  image: registry.gitlab.com/dependabot-gitlab/ci-images/release-cli:0.22
  needs:
    - pages
  variables:
    RELEASE_NOTES_FILE: release_notes.md
  script:
    - .gitlab/ci/script/changelog.sh
  interruptible: false
  release:
    tag_name: $CI_COMMIT_TAG
    description: $RELEASE_NOTES_FILE

.bump-version:
  stage: release
  image: registry.gitlab.com/dependabot-gitlab/ci-images/helm-helmdocs:3.17-1.14
  before_script:
    - source .gitlab/ci/script/utils.sh
    - setup_git "$CI_DEFAULT_BRANCH"
  script:
    - script/release.sh $VERSION_COMPONENT
    - git push origin "$CI_DEFAULT_BRANCH"
    - git push origin --tags
  interruptible: false
