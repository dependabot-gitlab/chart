{{/*
Updater pod template responsible for running dependency updates
*/}}
{{- define "dependabot-gitlab.updater-pod" -}}
{{- $hasExtraVolumes := or .Values.updater.extraVolumes (include "dependabot-gitlab.base-config" .) .Values.gitlabSslCert.existingSecret .Values.gitlabSslCert.cert -}}
metadata:
  name: {{ include "dependabot-gitlab.fullname" . }}-updater-%{name_sha}
  namespace: {{ .Release.Namespace | quote }}
  labels:
    {{- include "dependabot-gitlab.selectorLabels" . | nindent 4 }}
    {{- with .Values.updater.podLabels }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
    app.kubernetes.io/component: updater
  {{- with .Values.updater.podAnnotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  restartPolicy: Never
  {{- with .Values.updater.hostAliases }}
  hostAliases:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  serviceAccountName: {{ include "dependabot-gitlab.serviceAccountName" . }}
  {{- with .Values.podSecurityContext }}
  securityContext:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- if .Values.updater.activeDeadlineSeconds }}
  activeDeadlineSeconds: {{ .Values.updater.activeDeadlineSeconds }}
  {{- end }}
  containers:
    - name: {{ .Chart.Name }}-updater
      {{- with .Values.updater.containerSecurityContext }}
      securityContext:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- if .Values.updater.imagePattern }}
      image: {{ .Values.updater.imagePattern }}
      {{- else }}
      image: "{{ .Values.image.repository }}-%<package_ecosystem>s:{{ .Values.image.tag | default .Chart.AppVersion }}"
      {{- end }}
      imagePullPolicy: {{ .Values.image.pullPolicy }}
      args:
        - "rake"
        - "%<rake_task>s"
      env:
        - name: SETTINGS__METRICS
          value: "false"
      {{- with (include "dependabot-gitlab.database-credentials" .) }}
        {{- . | nindent 8 }}
      {{- end }}
      {{- with .Values.updater.extraEnvVars }}
        {{- toYaml . | nindent 8 }}
      {{- end}}
      envFrom:
        - configMapRef:
            name: {{ include "dependabot-gitlab.fullname" . }}
        - secretRef:
            {{- if .Values.credentials.existingSecret }}
            name: {{ .Values.credentials.existingSecret }}
            {{- else }}
            name: {{ include "dependabot-gitlab.fullname" . }}
            {{- end }}
        {{- if (include "dependabot-gitlab.registries-credentials" .) }}
        - secretRef:
            {{- if .Values.registriesCredentials.existingSecret }}
            name: {{ .Values.registriesCredentials.existingSecret }}
            {{- else }}
            name: {{ include "dependabot-gitlab.fullname" . }}-registries
            {{- end }}
        {{- end }}
      {{- with .Values.updater.lifecycle }}
      lifecycle:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.updater.resources }}
      resources:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- if $hasExtraVolumes }}
      volumeMounts:
      {{- if (include "dependabot-gitlab.base-config" .) }}
        {{- include "dependabot-gitlab.base-config-mount" . | nindent 8 }}
      {{- end }}
      {{- with .Values.updater.extraVolumeMounts }}
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- if or .Values.gitlabSslCert.existingSecret .Values.gitlabSslCert.cert }}
        {{- include "dependabot-gitlab.ssl-cert-mount" . | nindent 8 }}
      {{- end }}
      {{- end }}
  {{- with .Values.updater.nodeSelector }}
  nodeSelector:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- with .Values.updater.affinity }}
  affinity:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- with .Values.updater.tolerations }}
  tolerations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- with .Values.image.imagePullSecrets }}
  imagePullSecrets:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- if $hasExtraVolumes }}
  volumes:
  {{- if (include "dependabot-gitlab.base-config" .) }}
    {{- include "dependabot-gitlab.base-config-volume" . | nindent 4 }}
  {{- end }}
  {{- with .Values.updater.extraVolumes }}
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- if or .Values.gitlabSslCert.existingSecret .Values.gitlabSslCert.cert }}
    {{- include "dependabot-gitlab.ssl-cert-volume" . | nindent 4 }}
  {{- end }}
  {{- end }}
{{- end }}
